Machine Learning
================

Preprocessing
-------------
Explain how, why, and when doing preprocessing.

Night Cuts
..........
Pictures with a mean brightness of under a specified ``threshold`` will be
thrown away, because there is not enough information on them.

Color Cuts
..........
A cut is applied on the remaining pictures, which cuts out green-like and
red-like colors, so that only white, gray and blue colors are kept.

Neural Net
..........
Using ``keras.ImageDataGenerator`` the pictures are splitted into train and
test directories with subdirectories according to their label.

Random Forest
.............
The pictures are being binned.
These are the parameters for the random forest.

Training
--------

Evaluation
----------
