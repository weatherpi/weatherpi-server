.. weatherpi documentation master file, created by
   sphinx-quickstart on Sun Aug 19 11:45:17 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to weatherpi's documentation!
=====================================

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   weatherstation
   machinelearning
   api
