from predictions.models.neuralnet import Network
from predictions.models.randomforest import Random_forest
from predictions.models.evaluate import Evaluation

from predictions.preprocessing.prepro_data import preprocessor
from predictions.preprocessing.prepro_rf import prepro_rf
from predictions.preprocessing.cutter import RainbowCutter

from keras.layers import GaussianNoise, BatchNormalization
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D, AveragePooling2D

import numpy as np
import os
import shutil

if not os.path.exists("./build/"):
    os.makedirs("./build/")

full_data = "data/"
data_dir = "clouds_per_label/"
prepro_dir = "preprocessed_photos/"
label_path = "label.pkl"


def preprocess():
    pre = preprocessor(output_dir=prepro_dir, im_shape=(256,256))
    pre.clean_nights(full_data)
    
    print('Prepro clean nights done')

    if os.path.exists(data_dir):
        shutil.rmtree(data_dir)
    X, y = pre.gen_label_split(label_path, 95, 300)
    pre.save_splits(X, y, data_dir, tt_split=True)

    cut = RainbowCutter()
    pre_rf = prepro_rf(cut)

    for d in ["train/", "test/"]:
        X, y, filename = pre_rf.preprocess_dir(data_dir + d, bins=30)
        pre_rf.save(X, y, filename, data_dir + d)


def train_nn():
    batch_size = 128
    epochs = 500
    dim = (256,256)

    net = Network(dim=dim, batch_size=batch_size, epochs=epochs)

    # net.add_layer(
    #     Conv2D(3, kernel_size=(3, 3), padding="valid", strides=4, activation="relu", input_shape=(256, 256, 3))
    # )
    # net.add_layer(
    #     Conv2D(12, kernel_size=(5, 5), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    net.add_layer(
        Conv2D(3, kernel_size=(3, 3), padding="valid", strides=4, activation="relu", input_shape=(256, 256, 3))
    )
    net.add_layer(
        Conv2D(32, kernel_size=(5, 5), padding="valid", strides=1, activation="relu")
    )
    net.add_layer(MaxPooling2D(5, 5))
    net.add_layer(
        Conv2D(128, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    )
    net.add_layer(MaxPooling2D(3, 3))
    net.add_layer(Flatten())
    net.add_layer(Dropout(0.5))
    net.add_layer(GaussianNoise(0.3))
    net.add_layer(Dense(64, activation="relu"))
    net.add_layer(Dropout(0.20))
    net.add_layer(GaussianNoise(0.2))
    net.add_layer(Dense(16, activation="relu"))
    net.add_layer(Dropout(0.20))
    net.add_layer(GaussianNoise(0.2))
    net.add_layer(Dense(9, activation="softmax"))

    net.compile(loss="logcosh", optimizer="adam")
    
    net.fit(data_dir + "train/", data_dir + "test/")

    net.save("./build/")


def train_rf():
    rf = Random_forest(
        n_estimators=300, parameters=dict(max_depth=None, criterion="gini")
    )

    train_data = np.load(data_dir + "train/hist_dataset.npz")

    rf.fit(train_data["X"], train_data["y"])

    rf.save("./build/")


def evaluate():
    # --- RF ---
    # train_data = np.load(data_dir + 'train/hist_dataset.npz')
    test_data = np.load(data_dir + "test/hist_dataset.npz")
    rf = Random_forest()
    rf.load("./build/")

    evaluate_rf = Evaluation(rf, "Random Forest")
    y_pred_rf = rf.predict(test_data["X"])
    evaluate_rf.conf_mat(test_data["y"], y_pred_rf, "./build/conf_rf.pdf")
    evaluate_rf.training_RF("./build/", "./build/")

    # print('(random forest) Train score: {:.2f}'.format(rf.score(train_data['X'], train_data['y'])))
    print(
        "(random forest) Test score: {:.2f}".format(
            rf.score(test_data["X"], test_data["y"])
        )
    )

    # --- NN ---
    net = Network()
    net.load('./build/')
    evaluate_nn = Evaluation(net, 'CNN')
    evaluate_nn.training_NN('./build/network.pkl', './build/train_nn.pdf')
    gen = Dataset(shuffle=False, im_shape=(256,256))
    train_gen = gen.train_generator(data_dir + 'test/')

    label_encoder_nn = {y: x for x, y in train_gen.class_indices.items()}

    y_true = [
        label_encoder_nn[x]
        for x in net.model.predict_generator(train_gen).argmax(axis=1)
    ]
    y_pred = [label_encoder_nn[x] for x in train_gen.classes]

    evaluate_nn.conf_mat(y_true, y_pred, "./build/conf_nn.pdf")

    time_mean, time_std = evaluate_nn.compare_pre_time(
        [net, rf],
        '/home/maxsac/Pictures/preprocessed',
    )
    print(time_mean, time_std)
    evaluate_nn.plot_time(time_mean, time_std, ['CNN','RF'], './build/')


def pipeline(full_pipeline=False):
    if full_pipeline:
        print("This is the complete pipeline for the neural net and random forest.")
        print("\n========================================================")
        print("1. Preprocessing")
        print("========================================================\n")
        preprocess()
        print("\n========================================================")
        print("2. Training of Neural Net")
        print("========================================================\n")
        train_nn()
        print("\n========================================================")
        print("3. Training of Random Forest")
        print("========================================================\n")
        train_rf()
        print("\n========================================================")
        print("4. Evaluation of both models")
        print("========================================================\n")
        evaluate()
    else:
        i = 0
        ans_1 = input("Preprocess data? (y/[n]) ")
        ans_2 = input("Train neural net? (y/[n]) ")
        ans_3 = input("Train random forest? (y/[n]) ")
        ans_4 = input("Evaluate models? (y/[n]) ")
        if ans_1 == "y":
            i += 1
            print("\n========================================================")
            print("{}. Preprocessing".format(i))
            print("========================================================\n")
            preprocess()
        if ans_2 == "y":
            i += 1
            print("\n========================================================")
            print("{}. Training of Neural Net".format(i))
            print("========================================================\n")
            train_nn()
        if ans_3 == "y":
            i += 1
            print("\n========================================================")
            print("{}. Training of Random Forest".format(i))
            print("========================================================\n")
            train_rf()
        if ans_4 == "y":
            i += 1
            print("\n========================================================")
            print("{}. Evaluation of both models".format(i))
            print("========================================================\n")
            evaluate()


if __name__ == "__main__":
    ans = input(
        "Run full pipeline including preprocessing, trainig and evaluation of both models? (y/[n]) "
    )
    if ans == "y":
        pipeline(True)
    else:
        pipeline(False)
    print("\n========================================================")
    print("Thanks for your time. Come back soon :-)")
    print("========================================================\n")
