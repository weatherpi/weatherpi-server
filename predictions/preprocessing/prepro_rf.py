import numpy as np
from scipy.misc import imread
import os
from multiprocessing import Pool
from itertools import repeat
from predictions.preprocessing.cutter import RainbowCutter


class prepro_rf:
    def __init__(self, pic_filter=None):
        self.processes = 4
        self.pic_filter = pic_filter

    def hist_pic(self, img, bins=10):
        """Make three histogramm on the color channels with
        given bin

        Parameters
        ----------
        img: ndarray, (768, 1024, 3)
            a array with all 3 dim colorchannels are needed
        bins: int, 10
            number of bins for each color

        Returns
        -------
        ndarray
            3 dim histogramm for colorchannels
        """
        if self.pic_filter != None:
            img = self.pic_filter.cut_image(img)
        red, _bins = np.histogram(img[:, :, 0], bins=bins, range=[1, 256], density=True)
        yellow, _bins = np.histogram(
            img[:, :, 1], bins=bins, range=[1, 256], density=True
        )
        green, _bins = np.histogram(
            img[:, :, 2], bins=bins, range=[1, 256], density=True
        )
        return np.array([*red, *yellow, *green])

    def preprocess_label(self, label, data_dir, bins):
        """Preprocess one label. For one label histogramms are caculated.

        Parameters
        ----------
        label: string
            label of preprocessed data
        data_dir: string
            dir where data to find
        bins: int
            number of bins
        """
        pic_path = data_dir + "/" + label + "/"
        X = []
        filename = []
        for pic in os.listdir(pic_path):
            im = imread(pic_path + pic, flatten=False, mode="RGB")
            X.append(self.hist_pic(im, bins))
            filename.append(pic)
        return np.array(X), np.array([label] * len(X)), filename

    def preprocess_dir(self, data_dir, bins=10):
        """Preprocess all label in dataset and append to a multidimensional
        array.

        Parameters
        ----------
        data_dir: string
            dir where data to find
        bins: int, 10
            number of bins
        data_dir: string
        """
        print("(prepro_rf) Generate histogramms for data")
        pool = Pool(processes=self.processes)
        Result = pool.starmap_async(
            self.preprocess_label,
            zip(os.listdir(data_dir), repeat(data_dir), repeat(bins)),
        )
        data = np.array(Result.get())
        X = np.array(data[0][0])
        y = np.array(data[0][1])
        filename = np.array(data[0][2])
        for X_label, y_label, f_name in data[1:]:
            X = np.vstack((X, X_label))
            y = np.append(y, y_label)
            filename = np.append(filename, f_name)
        return X, y, filename

    def save(self, X, y, filename, to_path):
        """Preprocess one label. For one label histogramms are caculated.

        Parameters
        ----------
        (X, y, filename): (ndarray, np.array, np.array,)
            data with label and filenames
        to_path: string
            location to store .npz
        """
        npz_path = to_path + "hist_dataset"
        print("(prepro_rf) save zipped data to {}.npz".format(npz_path))
        np.savez_compressed(npz_path, X=X, y=y, filename=filename)


def main():
    cut = RainbowCutter()
    pre = prepro_rf(cut)

    X, y, filename = pre.preprocess_dir("../clouds_per_label/train/", bins=30)
    pre.save(X, y, filename, "../clouds_per_label/train/")

    X, y, filename = pre.preprocess_dir("../clouds_per_label/test/", bins=30)
    pre.save(X, y, filename, "../clouds_per_label/test/")


if __name__ == "__main__":
    main()
