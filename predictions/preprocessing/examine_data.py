import numpy as np
import pandas as pd
from scipy.misc import imread
import predictions.preprocessing.prepro_data as pre_data
import predictions.preprocessing.prepro_rf as pre_rf
import predictions.models.randomforest as rf
from predictions.models.neuralnet import Network, Dataset
from multiprocessing import Pool
from itertools import repeat

class examine_data:
    def __init__(self, model, label_file):
        ''' Load a given modell to compare predictions of modell with 
        labels in label_file.
        Parameters:
        model: predicitions.model
            A model of class weatherpi for predicting data.
        label_file: string
            File to label.pkl to compare with predictions of modell.
        '''
        self.model = model
        self.df_label = pd.read_pickle(label_file)
        self.df_label = self.df_label[self.df_label.final_label != 'bad picture']

    def check(self, y_pred, y_label, filename, to_do_path):
        ''' Check if given predicitions compare with labelfile.
        If not, create a new DataFrame with filenames witch doesn't match.
        Parameters:
        X: ndarray
            data to classified
        y_label: array
            label set in the DataFrame
        filename: string
            path to label.pkl
        to_do_path:
            path to to_check DataFrame
        '''
        file_to_check = filename[y_label != y_pred]
        print('(examine data) {} data checked, {} need a recheck by persons'.format(
            len(y_pred), len(file_to_check)))
        df = pd.DataFrame({'filename': file_to_check})
        df.to_pickle(to_do_path)

def main():
    ranf = rf.Random_forest()
    ranf.load('../pipeline/build/')

    data = np.load('../pipeline/clouds_per_label/test/hist_dataset.npz')
    y_pred = ranf.predict(data['X'])
    exam = examine_data(ranf, '/home/maxsac/Documents/weatherpi/weatherpi/weatherpi_bot/label.pkl')
    exam.check(y_pred, data['y'], data['filename'], './to_do.pkl')

    # net = Network()
    # net.load('../pipeline/build/')

    # DataDir = '../pipeline/clouds_per_label/test/'
    # pre_d =  pre_data.preprocessor(None)
    # X_path, y_label = pre_d.encode_dir(DataDir)
    # y_pred = [] 
    # for x, label in zip(X_path, y_label):
    #     y_pred.append(net.predict_photo(DataDir +'/' + label + '/' + x).argmax(axis=1))

if __name__ == '__main__':
    main()
