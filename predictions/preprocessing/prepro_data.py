from glob import glob
import numpy as np
import pandas as pd
from multiprocessing import Pool
from itertools import repeat
from shutil import copy
import os
from sklearn.model_selection import train_test_split
from PIL import Image


class preprocessor:
    def __init__(self, output_dir, im_shape=None, threshold=70):
        """Initalize preprocessor wich handle the labe of pictures.

        Parameters
        ----------
        output_dir: string
            dir to save preprocessed data
        im_shape: tuple, None
        threshold: int, 70
            mean color of picture with will be discard
        """
        self.output_dir = output_dir
        self.threshold = threshold
        self.im_shape = im_shape
        self.processes = 4

    def load_pic(self, filename):
        """Load picture from given filename.

        Parameters
        ----------
        filename: string
            location of file to open

        Returns
        -------
        ndarray
            picture in form of array
        """
        return np.asarray(Image.open(filename), dtype=np.int16)

    def clean_night(self, filename):
        """Discard Picture if it is lower than threshold, else copy to
        preprocessing dir.

        Parameters
        ----------
        filename: string
            filename of picture to check
        """
        des_filename = filename.replace(self.preprocess_dir, "")
        des_filename = str(self.output_dir + des_filename)
        pic = self.load_pic(filename)
        if np.mean(pic) > self.threshold:
            copy(filename, des_filename)

    def clean_nights(self, preprocess_dir):
        """Check a whole dir for nights.

        Parameters
        ----------
        preprocess_dir: string
            dir to preprocess
        """
        print("(prepro_data) Cleaning nights", end=" ... ")
        #filelist = glob(preprocess_dir + "*.jpg")
        filelist = glob(preprocess_dir + "*")
        self.preprocess_dir = preprocess_dir
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        pool = Pool(processes=self.processes)
        result = pool.map_async(self.clean_night, filelist)
        result.wait()
        print("Done")

    def cut_label(self, label, df, min_feature_size, max_feature_size):
        """Cut label to given border to reduce overfitting on frequently
        classes.

        Parameters
        ----------
        label: string
            label which should be cutted
        df: DataFrame
            location of filename and labels
        min/max_feature_size: int
            range of imagenumber to keep

        Returns
        -------
        (array, array)
            cutted tuple of label and events of adjustied size
        """
        mask = df.final_label == label
        if np.sum(mask) >= min_feature_size:
            events = np.random.choice(df.filename[mask].values, size=max_feature_size)
            return label, events
        else:
            return label, np.array([None])

    def gen_label_split(self, path_label, min_feature_size=100, max_feature_size=100):
        """Cut label to given border to reduce overfitting on frequently
        classes.

        Parameters
        ----------
        path_label: string
            location of filename and labels
        min_feature_size, max_feature_size: int, 100
            range of imagenumber to keep

        Returns
        -------
        (array, array)
            cutted tuple of label and events of adjustied size
        """
        df = pd.read_pickle(path_label).dropna()
        not_bad_pic = df["final_label"] != "bad picture"
        df = df[not_bad_pic]

        label = np.unique(df.final_label.values)
        pool = Pool(processes=self.processes)
        Result = pool.starmap_async(
            self.cut_label,
            zip(label, repeat(df), repeat(min_feature_size), repeat(max_feature_size)),
        )
        data = np.array(Result.get())

        X = np.array([])
        y = np.array([])
        n_classes = 0
        for label, l_data in data:
            mask = l_data == None
            if sum(mask) == 0:
                X = np.append(X, l_data)
                y = np.append(y, [label] * len(l_data))
                n_classes += 1
        print("(prepro_data) Dataset includes {} classes.".format(n_classes))

        return X, y

    def save_data(self, X, y, to_path):
        """Copy preprocessed Data to output_dir

        Parameters
        ----------
        X: string
            filename of image
        y: string
            label of image
        to_path: string
            destination dir
        """
        if self.im_shape == None:
            copy(self.output_dir + X, to_path + y + "/" + X)
        else:
            img = Image.open(self.output_dir + X)
            img = img.resize(self.im_shape)
            img.save(to_path + y + "/" + X)

    def save_splits(self, X, y, to_path, tt_split=True, t_size=0.2):
        """Save a Dataset in a dir per label.

        Parameters
        ----------
        X: string
            filename of image
        y: string
            label of image
        to_path: string
            destination dir
        tt_split: bool, True
            should a test set be generated
        t_size: double, 0.2
            size of the testset
        """
        work_to_do = True
        if tt_split == True:
            X_train, X_test, y_train, y_test = train_test_split(
                X, y, stratify=y, test_size=t_size, random_state=2702
            )
            self.save_splits(X_train, y_train, to_path + "train/", False)
            self.save_splits(X_test, y_test, to_path + "test/", False)
            work_to_do = False

        if work_to_do == True:
            for label in np.unique(y):
                if not os.path.exists(to_path + label):
                    os.makedirs(to_path + label)
                else:
                    print(
                        "(prepro_data) {} already exist, please remove it by your "
                        "own to ensure that wrong data will be received".format(
                            to_path + label
                        )
                    )
            print("(prepro_data) Save Data to {}".format(to_path), end=" ... ")

            pool = Pool(processes=self.processes)
            result = pool.starmap_async(self.save_data, zip(X, y, repeat(to_path)))
            result.get()
            print("Done")

    def encode_dir(self, data_path):
        """Encode the label of a dir.

        Moeglicherweise ist die Klasse ueberholt (have to be checked.)

        Parameters
        ----------
        data_path: string

        Returns
        -------
        data, y_true
            data, y_true
        """
        classes = os.listdir(data_path)
        data = []
        y_true = []
        for klasse in classes:
            n_path = os.path.join(data_path, klasse)
            if os.path.isdir(n_path):
                file_dir = os.listdir(n_path)
                for File in file_dir:
                    # data.append(os.path.join(n_path, File))
                    data.append(File)
                    y_true.append(klasse)
        return data, y_true


def main():
    pre = preprocessor(
        output_dir="/home/maxsac/Pictures/preprocessed/", im_shape=(256, 256)
    )
    pre.clean_nights("/home/maxsac/Pictures/clouds/")
    # X, y = pre.gen_label_split(
    #         "/home/maxsac/Documents/weatherpi/weatherpi/weatherpi_bot/label.pkl",
    #          80, 150,
    #         )
    # pre.save_splits(X, y, '../clouds_per_label/', tt_split=True)


if __name__ == "__main__":
    main()
