import numpy as np
from fire import Fire
from imageio import imwrite, imread
import matplotlib as mpl

mpl.use("Agg")
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class RainbowCutter:
    def __init__(
        self,
        N=256,
        theta=-np.pi / 3.5,
        scale=-10,
        xshift=1.9,
        yshift=1.9,
        xbias=-0.1,
        ybias=0.07,
    ):
        """Initalize a 3d meshgrid with N points per axis.

        Parameters
        ----------
        N : int
            Discretisation of axes
        theta : float
            Rotation angel 0..2pi
        yshift : float
        xbias : float
        ybias : float
        """
        self.theta = theta
        self.scale = scale
        self.xshift = xshift
        self.yshift = yshift
        self.xbias = xbias
        self.ybias = ybias

    def _rotate(self, x, y, theta):
        """Rotate x and y tuple by theta at coordinate origin.

        Parameters
        ----------
        x, y : np.ndarray
            Arrays to rotate
        theta : float
            Rotation angle

        Returns
        -------
        np.ndarray, np.ndarray
            Rotated input arrays
        """
        xr = np.cos(theta) * x + np.sin(theta) * y
        yr = -np.sin(theta) * x + np.cos(theta) * y
        return xr, yr

    def cut_function(self, x, y, z):
        """Separate background from signal.

        Parameters
        ----------
        x, y, z: np.ndarray
            RGB colors

        Returns
        -------
        np.ndarray
            True/False array containing information about
            which points to keep or discard.
        """
        x0 = z * self.xshift + self.xbias
        y0 = z * self.yshift + self.ybias
        x_rot, y_rot = self._rotate(x, y, self.theta)
        x0_rot, y0_rot = self._rotate(x0, y0, self.theta)
        x = x_rot - x0_rot
        y = y_rot - y0_rot
        mask = y < self.scale * (x * x)
        return mask

    def cut_image(self, im):
        """Apply mask to image.

        Parameters
        ----------
        im : np.ndarray(PIL.Image.open())
            png/jpg-image to cut

        Returns
        -------
        np.ndarray
            Cutted image
        """
        im = im / 255.0
        r = im[:, :, 0]
        g = im[:, :, 1]
        b = im[:, :, 2]
        mask = self.cut_function(r, g, b)
        im[~mask] = 0
        im = im * 255.0
        return im

    def cut_image_and_save(self, img_path, save_path):
        """Apply cuts on a given Image and save it.

        Parameters
        ----------
        img_path : string
            image to load
        save_path : string
            location to store image
        """
        img = imread(img_path)
        img = self.cut_image(img)
        img = img.astype(np.uint8)
        imwrite(save_path, img)

    def plot_colorcube(self, save_path, n_points=50):
        """Plot a 3dim colorcube and applt cuts on it.

        Parameters
        ----------
        save_path : string
            location to store image
        n_points : int
            pixel density of image
        """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        r, b, g = np.meshgrid(
            np.linspace(0, 1, n_points),
            np.linspace(0, 1, n_points),
            np.linspace(0, 1, n_points),
        )
        rgb = np.array([r.flatten(), g.flatten(), b.flatten()]).T
        mask = self.cut_function(r, g, b)
        ax.scatter(
            255 * r[mask],
            255 * b[mask],
            255 * g[mask],
            c=rgb[mask.flatten()],
            marker="o",
        )
        ax.set_xlabel("Rot")
        ax.set_ylabel("Blau")
        ax.set_zlabel("Gruen")
        plt.tight_layout(pad=0)
        plt.savefig(save_path)
        plt.clf()


def main():
    cutter = RainbowCutter()
    Fire(cutter.cut_image_and_save)


if __name__ == "__main__":
    main()
