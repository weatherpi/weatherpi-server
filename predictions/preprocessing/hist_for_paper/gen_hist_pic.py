import numpy as np
from scipy.misc import imread
from predictions.preprocessing.prepro_rf import prepro_rf
from predictions.preprocessing.cutter import RainbowCutter
import matplotlib.pyplot as plt

bins = 10
width = 255/(4*bins)
print(width)
cut = RainbowCutter()
img = imread('./cirrostratus.jpg')
fig = plt.figure(figsize=(9,4))
for pre, i in zip([prepro_rf(),prepro_rf(cut,)],[1,3]):
    ax = fig.add_subplot('22{}'.format(i))
    hists = np.array(pre.hist_pic(img, bins=bins))
    red = hists[0:bins-1]
    blue = hists[bins:2*bins-1]
    green = hists[2*bins:3*bins-1]
    ax.bar(np.linspace(0*width, 255, len(red)), red, width, color='r')
    ax.bar(np.linspace(1*width, 255+1*width, len(blue)), blue, width, color='b')
    ax.bar(np.linspace(2*width, 255+2*width, len(green)), green, width, color='g')
    if i == 1:
        plt.setp(ax.get_xticklabels(), visible=False)
ax = fig.add_subplot('222')
ax.axis('off')
ax.imshow(img/255)
ax = fig.add_subplot('224')
ax.imshow(cut.cut_image(img)/255)
ax.axis('off')
plt.tight_layout(pad=0)
plt.savefig('cut_hist.pdf')
plt.clf()
print('Done')
