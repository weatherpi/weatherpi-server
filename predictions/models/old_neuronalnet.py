#from keras.utils import np_utils
import keras
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
import numpy as np
from time import sleep

from keras.preprocessing.image import ImageDataGenerator


class Dataset:
    def __init__(self, rescale=True, batch_size=1, shuffle=True, im_shape=(256, 256)):
        """A generator to handle data for neural network.

        Parameters
        ----------
        rescale: bool, True
            Scale data with maximal pixelvalue if true.
        batch_size: int, 1
            batch size of loaded data, in best case same as network.
        shuffel: bool, True
            Variable if the generator mix data intern.
        im_shape: tuple, (768, 1024)
            Input dimension of pictures.
        """
        if rescale:
            self.scale = 1.0 / 255.0
        else:
            self.scale = 1.0
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.im_shape = im_shape

    def train_generator(self, data_path, val=False):
        """Generator to train a NN.

        Reduce RAM storage through sequential loading data from hard-disk.

        Parameter
        ---------
        data_path: string
            Path to data dir. For training a dir for every label is required.
        val: bool
            Bool if the generator make a validations split by his own.

        Returns
        -------
        ImageDataGenerator
            Generate a tuple of trainingsdata and label for given batchsize.
        """
        return ImageDataGenerator(
            rescale=self.scale,
            # validation_split=0.2,
        ).flow_from_directory(
            data_path,
            target_size=self.im_shape,
            batch_size=self.batch_size,
            class_mode='categorical',
            seed=42,
            shuffle=self.shuffle,
            # subset='validation' if val else 'training',
        )

    def encode_label(self, gen, y_pred):
        """Encode the predicitions to primary label and give primary label back.

        Parameter
        ---------
        gen: ImageDataGenerator
            flow of a IMG
        y_pred: ndarray
            sparsed predictionvalues of network.

        Returns
        -------
        array, array
            Encoded labels for predicted and true label.
        """
        y_pred = y_pred.argmax(axis=1)
        y_true = gen.classes
        label_encoder = gen.class_indices
        label_encoder = {y: x for x, y in label_encoder.items()}
        y_pred = [label_encoder[label] for label in y_pred]
        y_true = [label_encoder[label] for label in y_true]
        return y_pred, y_true


import pandas as pd
from keras.models import Sequential
from keras.layers import GaussianNoise
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D, AveragePooling2D
from keras.utils import plot_model
from keras.metrics import categorical_accuracy
from keras.models import model_from_yaml
from keras import optimizers
from keras import regularizers
from PIL import Image


class Network:
    def __init__(self, batch_size=8, epochs=1):
        """Initalize a sequential neuronal network.

        Parameters
        ----------
        batch_size: int, 8
            Batch size of neuronal network.
        epochs: int, 8
            Number of training epochs.
        """
        self.model = Sequential()
        self.batch_size = batch_size
        self.epochs = epochs
        self.history = None
        self.label = []

    def add_layer(self, layer):
        """

        Parameters
        ----------
        layer: keras.layer
            Add a layer to sequential model.
        """
        self.model.add(layer)

    def compile(
        self,
        loss="categorical_crossentropy",
        optimizer="adam",
        metrics=[categorical_accuracy],
        verbose=True,
    ):
        """Compile sequential model.

        Parameters
        ----------
        loss: keras.losses (str), "categorical_crossentropy"
            Training and evaluation loss.
        optimizer: keras.optimizers (str), "adam"
            Used optimizer for training.
        metrics: keras.metrics (list), categorical_accuracy
            Metric to evaluated model.
        verbose: bool, True
            Value if model summary should be shown.
        """
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        if verbose:
            self.model.summary()

    def fit(self, train_gn, val_gn):
        """Fit neuronal network and save traininghistory.

        Parameters
        ----------
        train_gn: ImageDataGenerator
            Generator of training data.
        val_gn: ImageDataGenerator
            Generator of validation data.
        """
        print("(neuronal network) train model with data")
        callback = self.model.fit_generator(
            train_gn, validation_data=val_gn, epochs=self.epochs, verbose=True,
            steps_per_epoch=10, validation_steps=3
        )
        self.history = pd.DataFrame(callback.history)
        self.label = [*train_gn.class_indices.keys()]

    def evaluate(self, test_gn):
        """Evaluate trained neuronal network.

        Parameters
        ----------
        test_gn: ImageDataGenerator
            Generator of test data.

        Returns
        -------
        dict
            Dict of loss and metrics on evaluation data (not sure, have to
            be checked).
        """
        score = self.model.evaluate_generator(test_gn)
        return score.history

    def predict_proba(self, X):
        """Predict probabilities of given data.

        Parameters
        ----------
        X: ndarray
            Preprocessed trainingsdata.

        Returns
        -------
        np.array
            Not encoded label.
        """
        proba = self.model.predict(X)
        return proba, self.label 

    def predict(self, X):
        """Predict label of given data.

        Parameters
        ----------
        X: ndarray
            Preprocessed trainingsdata.

        Returns
        -------
        np.array
            Not encoded label.
        """
        proba, label = self.predict_proba(X)
        ind = np.argmax(proba)
        return label[ind], proba.flatten()[ind]

    def predict_photo(self, file_path):
        """Read photo from dir, predicted and give label back.

        Parameters
        ----------
        file_path: string
            Path of picture to predict.

        Returns
        -------
        int
            Not encoded label.
        """
        img = np.asarray(Image.open(file_path), dtype=np.int16)
        img = np.array(img / 255.).reshape(1, img.shape[0], img.shape[1], img.shape[2])
        m_cls, m_proba = self.predict(img)
        return m_cls, m_proba

    def save(self, file_path):
        """Save trained network and history to given path.

        Parameters
        ----------
        file_path: string
            Path to save data.
        """
        print("(neuronal network) Saved model to disk")
        model_yaml = self.model.to_yaml()
        with open(file_path + "network.yaml", "w") as yaml_file:
            yaml_file.write(model_yaml)
        self.model.save_weights(file_path + "network.h5")
        self.history.to_pickle(file_path + "network.pkl")
        np.save(file_path + "label.npy", self.label)

    def load(self, file_path, compile=True):
        """Load a trained network and compiled default.

        Parameters
        ----------
        file_path: string
            File to dir with saved model.
        compile: bool, True
            Value if compilation is part of loading process.
        """
        print("(neuronal network) Loaded model from disk")
        yaml_file = open(file_path + "network.yaml", "r")
        loaded_model_yaml = yaml_file.read()
        yaml_file.close()
        self.model = model_from_yaml(loaded_model_yaml)
        self.model.load_weights(file_path + "network.h5")
        self.label = np.load(file_path + "label.npy")
        if compile:
            self.compile(verbose=False)


def main():
    # batch_size = 32
    # epochs = 400
    # data = Dataset(batch_size=batch_size, im_shape=(256, 256))

    # train_gen = data.train_generator("../clouds_per_label/train/")
    # val_gen = data.train_generator("../clouds_per_label/test/")

    # net = Network(batch_size=batch_size, epochs=epochs)

    # net.add_layer(AveragePooling2D(pool_size=(3, 4), input_shape=(768, 1024, 3)))
    # net.add_layer(
    #     Conv2D(3, kernel_size=(3, 3), padding="valid", strides=4, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(12, kernel_size=(5, 5), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(Flatten())
    # net.add_layer(Dropout(0.5))
    # net.add_layer(GaussianNoise(0.15))
    # net.add_layer(Dense(32, activation="relu"))
    # net.add_layer(Dropout(0.2))
    # net.add_layer(GaussianNoise(0.15))
    # net.add_layer(Dense(32, activation="relu"))
    # net.add_layer(Dropout(0.2))
    # net.add_layer(Dense(9, activation="softmax"))

    # net.compile(loss="logcosh", optimizer="adam", metrics=[categorical_accuracy])
    # net.fit(train_gen, val_gen)
    # net.save("./build/")
    # net.load("./build/")
    net = Network()
    net.load('../pipeline/build/')
    cls, proba = net.predict_photo('../pipeline/clouds_per_label/train/cumulus/00000056.png')
    print(cls, proba)

if __name__ == "__main__":
    main()
