#from keras.utils import np_utils
import numpy as np
import pandas as pd
import keras
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import GaussianNoise
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D, AveragePooling2D
from keras.metrics import categorical_accuracy
from keras.models import model_from_yaml
from keras import optimizers

class Network:
    def __init__(self, dim=(256,256), batch_size=8, epochs=1):
        """Initalize a sequential neuronal network.

        Parameters
        ----------
        batch_size: int, 8
            Batch size of neuronal network.
        epochs: int, 8
            Number of training epochs.
        """
        self.model = Sequential()
        self.batch_size = batch_size
        self.epochs = epochs
        self.dim = dim
        self.history = None
        self.label = []
        self.gen = ImageDataGenerator(
            samplewise_center=True,
            samplewise_std_normalization=True,
            rescale=1./255,
          )

    def add_layer(self, layer):
        """

        Parameters
        ----------
        layer: keras.layer
            Add a layer to sequential model.
        """
        self.model.add(layer)

    def compile(self, loss="categorical_crossentropy", optimizer="adam",
        metrics=[categorical_accuracy], verbose=True):
        """Compile sequential model. and create a IMG_gen for prepro data.

        Parameters
        ----------
        loss: keras.losses (str), "categorical_crossentropy"
            Training and evaluation loss.
        optimizer: keras.optimizers (str), "adam"
            Used optimizer for training.
        metrics: keras.metrics (list), categorical_accuracy
            Metric to evaluated model.
        verbose: bool, True
            Value if model summary should be shown.
        """
        self.model.compile(loss=loss, optimizer=optimizer, metrics=metrics)
        if verbose:
            self.model.summary()

    def fit(self, train_path, val_path):
        """Fit neuronal network and save traininghistory.

        Parameters
        ----------
        train_path: ImageDataGenerator
            Path of training data.
        val_path: ImageDataGenerator
            Path of validation data.
        """
        print("(neuronal network) train model with data")
        train_gn = self.gen.flow_from_directory(
            train_path, target_size=self.dim,
            batch_size=self.batch_size,
        )
        val_gn = self.gen.flow_from_directory(
            val_path, target_size=self.dim,
            batch_size=self.batch_size,
        )
        # calculate how much steps are needed for complete dataset
        # int division so small rest exist
        training_steps = train_gn.n // train_gn.batch_size
        validation_steps = val_gn.n // val_gn.batch_size

        callback = self.model.fit_generator(
            train_gn, validation_data=val_gn, epochs=self.epochs, verbose=True,
            steps_per_epoch=training_steps, validation_steps=validation_steps
        )
        self.history = pd.DataFrame(callback.history)
        label = train_gn.class_indices.keys()
        self.label = [*label]

    def evaluate(self, test_gn):
        """Evaluate trained neuronal network.

        Parameters
        ----------
        test_gn: ImageDataGenerator
            Generator of test data.

        Returns
        -------
        dict
            Dict of loss and metrics on evaluation data (not sure, have to
            be checked).
        """
        score = self.model.evaluate_generator(test_gn)
        return score.history

    def predict_proba(self, X):
        """Predict probabilities of given data.

        Parameters
        ----------
        X: ndarray
            Preprocessed trainingsdata.

        Returns
        -------
        np.array
            Not encoded label.
        """
        proba = self.model.predict(X)
        return proba, self.label 

    def predict(self, X):
        """Predict label of given data.

        Parameters
        ----------
        X: ndarray
            Preprocessed trainingsdata.

        Returns
        -------
        np.array
            Not encoded label.
        """
        proba, label = self.predict_proba(X)
        ind = np.argmax(proba)
        return label[ind], proba.flatten()[ind]

    def predict_photo(self, file_path):
        """Read photo from dir, predicted and give label back.

        Parameters
        ----------
        file_path: string
            Path of picture to predict.

        Returns
        -------
        int
            Not encoded label.
        """
        img = load_img(file_path, target_size=self.dim)
        x = img_to_array(img)
        x = x.reshape((1,) + x.shape)
        x = self.gen.standardize(x)
        m_cls, m_proba = self.predict(x)
        return m_cls, m_proba

    def save(self, file_path):
        """Save trained network and history to given path.

        Parameters
        ----------
        file_path: string
            Path to save data.
        """
        print("(neuronal network) Saved model to disk")
        model_yaml = self.model.to_yaml()
        with open(file_path + "network.yaml", "w") as yaml_file:
            yaml_file.write(model_yaml)
        self.model.save_weights(file_path + "network.h5")
        self.history.to_pickle(file_path + "network.pkl")
        np.save(file_path + "label.npy", self.label)

    def load(self, file_path, compile_m=False):
        """Load a trained network and not compile by default.

        Parameters
        ----------
        file_path: string
            File to dir with saved model.
        compile: bool, True
            Value if compilation is part of loading process.
        """
        print("(neuronal network) Loaded model from disk")
        yaml_file = open(file_path + "network.yaml", "r")
        loaded_model_yaml = yaml_file.read()
        yaml_file.close()
        self.model = model_from_yaml(loaded_model_yaml)
        self.model.load_weights(file_path + "network.h5")
        self.label = np.load(file_path + "label.npy")
        if compile_m:
            self.compile(verbose=False)
        else:
            self.model._make_predict_function()


def main():
    # batch_size = 32
    # epochs = 400
    # data = Dataset(batch_size=batch_size, im_shape=(256, 256))

    # train_gen = data.train_generator("../clouds_per_label/train/")
    # val_gen = data.train_generator("../clouds_per_label/test/")

    # net = Network(batch_size=batch_size, epochs=epochs)

    # net.add_layer(AveragePooling2D(pool_size=(3, 4), input_shape=(768, 1024, 3)))
    # net.add_layer(
    #     Conv2D(3, kernel_size=(3, 3), padding="valid", strides=4, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(12, kernel_size=(5, 5), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(48, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(
    #     Conv2D(32, kernel_size=(3, 3), padding="valid", strides=1, activation="relu")
    # )
    # net.add_layer(MaxPooling2D(2, 2))
    # net.add_layer(Flatten())
    # net.add_layer(Dropout(0.5))
    # net.add_layer(GaussianNoise(0.15))
    # net.add_layer(Dense(32, activation="relu"))
    # net.add_layer(Dropout(0.2))
    # net.add_layer(GaussianNoise(0.15))
    # net.add_layer(Dense(32, activation="relu"))
    # net.add_layer(Dropout(0.2))
    # net.add_layer(Dense(9, activation="softmax"))

    # net.compile(loss="logcosh", optimizer="adam", metrics=[categorical_accuracy])
    # net.fit(train_gen, val_gen)
    # net.save("./build/")
    # net.load("./build/")
    net = Network()
    net.load('../pipeline/build/', compile_m=False)
    cls, proba = net.predict_photo('../pipeline/data/00000053.png')
    print(cls, proba)

if __name__ == "__main__":
    main()
