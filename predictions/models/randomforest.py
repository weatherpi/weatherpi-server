import numpy as np
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from predictions.preprocessing.prepro_rf import prepro_rf
from PIL import Image


class Random_forest:
    def __init__(
        self,
        n_estimators=10,
        n_jobs=-1,
        random_state=None,
        parameters=dict(),
        preprocessor=None,
    ):
        """Initalize a Random Forest and a preprocessor if needed.

        Parameters
        ----------
        n_estimators: int, 10
            Number of trees in forest.
        n_jobs: int, -1
            Number of physical cores to use for training.
            Default is -1 which means all cores.
        random_state: int or None, None
            Seed for random number generators.
            Default is None, which means no seed.
        parameters: dict
            Dictionary containing arguments for sklear.ensemble.RandomForestClassifier.
            Default is empty.
        preprocessor: preprocessor, None
            Default is None which means that prepro_rf is used, which means
            binning of colorvalues.
        """
        self.clf = RandomForestClassifier(
            n_estimators=n_estimators,
            n_jobs=n_jobs,
            **parameters,
            random_state=random_state,
        )
        self.preprocessor = preprocessor

    def fit(self, X_train, y_train):
        """Fit data to labels.

        Parameters
        ----------
        X_train: np.ndarray
            Train data.
        y_train: np.ndarray
            Labels.
        """
        print("(random forest) Training", end=" ... ")
        self.clf.fit(X_train, y_train)
        print("Done")

    def predict_proba(self, X):
        """Predict probabilities for given data.

        Parameters
        ----------
        X: np.ndarray
            Data.

        Returns
        -------
        cls, proba: float, float
            Label for X and proba.
        """
        print("(random forest) Predicting probalbilities", end=" ... ")
        y = self.clf.predict_proba(X)
        print("Done")
        return y, self.clf.classes_

    def predict(self, X):
        """Predict label of input samples.

        Parameters
        ----------
        X: np.ndarray
            Data.

        Returns
        -------
        cls, proba: float, float
            Label for X and proba.
        """
        proba, cls = self.predict_proba(X)
        ind = np.argmax(proba)
        m_proba = proba.flatten()[ind]
        m_cls = cls[ind]
        return m_cls, m_proba

    def predict_photo(self, file_path):
        """Predict label of photo.

        Parameters
        ----------
        file_path: str
            Path pointing to a ``.jpg`` or ``.png`` file.
            Should be a picture of clouds.

        Returns
        -------
        str
            Label for photo.
        """
        if self.preprocessor == None:
            self.preprocessor = prepro_rf()
        img = np.asarray(Image.open(file_path), dtype=np.int16)
        X = self.preprocessor.hist_pic(img, bins=30)
        cls, proba = self.predict(X.reshape(1, X.shape[0]))
        return cls, proba

    def score(self, X_test, y_test):
        """Get accuracy score of Random Forest.

        Parameters
        ----------
        X_test: np.ndarray
            Input data.
        y_test: np.ndarray
            Input labels.

        Returns
        -------
        float
            Accuracy of Random Forest on data and provided labels.
        """
        return self.clf.score(X_test, y_test)

    def save(self, path):
        """Persistantly save Random Forest model to file.

        Save model as ``rf.pkl`` file.

        Parameters
        ----------
        path: string
            Path to directory to save model to.
        """
        print("(random forest) Save model")
        joblib.dump(self.clf, path + "rf.pkl")
        importance = self.clf.feature_importances_
        std = np.std(
            [tree.feature_importances_ for tree in self.clf.estimators_], axis=0
        )
        feature = []
        n = len(importance) / 3
        for color in np.array(["r", "g", "b"]):
            for bin_lower, bin_upper in zip(
                np.linspace(1, 255 * (1 - 1 / n), n), np.linspace(255 * (1 / n), 255, n)
            ):
                feature.append(
                    color + "_" + str(int(bin_lower)) + "_" + str(int(bin_upper))
                )

        np.savez_compressed(
            path + "feature_importances",
            importance=importance,
            std=std,
            feature=feature,
        )

    def load(self, path):
        """Load a Random Forest model.

        Save model as ``self.clf``.
        """
        print("(random forest) Loading model")
        self.clf = joblib.load(path + "rf.pkl")


def main():
    rf = Random_forest(
        n_estimators=300, parameters=dict(max_depth=None, criterion="gini")
    )

    train_data = np.load("../clouds_per_label/train/hist_dataset.npz")
    rf.fit(train_data["X"], train_data["y"])

    test_data = np.load("../clouds_per_label/test/hist_dataset.npz")
    print(
        "(random forest) Train score: {:.2f}".format(
            rf.score(train_data["X"], train_data["y"])
        )
    )
    print(
        "(random forest) Test score: {:.2f}".format(
            rf.score(test_data["X"], test_data["y"])
        )
    )
    rf.save("./build/")


if __name__ == "__main__":
    main()
