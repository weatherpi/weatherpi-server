import numpy as np
import pandas as pd
from predictions.preprocessing.prepro_data import preprocessor
from predictions.models.randomforest import Random_forest
from predictions.models.neuralnet import Network
from sklearn.metrics import roc_curve, confusion_matrix
from os import listdir, path
import itertools
import time
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
from sklearn import preprocessing

class Evaluation:
    def __init__(self, model, model_name):
        self.model = model
        self.model_name = model_name


    def conf_mat(self, y_true, y_pred, savepath):
        """Plot confusion matrix.

        Parameters:
            y_true: np.array
                True labels.
            y_pred: np.array / list
                Predicted labels.
        """
        cm = confusion_matrix(y_true, y_pred)
        cm = 100*cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    
        cmap=plt.cm.Blues
        plt.figure(figsize=(4.0, 3.0))
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.colorbar()
        classes = np.unique(y_true)
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45, rotation_mode="anchor", 
                horizontalalignment="right")
        plt.yticks(tick_marks, classes)
        fmt = '.0f'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                horizontalalignment="center",
                verticalalignment="center",
                color="white" if cm[i, j] > thresh else "black")
    
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.tight_layout(pad=0)
        plt.savefig(savepath)
        plt.clf()

    def training_NN(self, history_path, savepath):
        print('(evaluation) Plot training history of CNN')
        history = pd.read_pickle(history_path)
        epochs = len(history)

        fig = plt.figure(figsize=(4.4,2.7))
        ax = fig.add_subplot(111)
        ax.plot(range(epochs), history.val_loss, 'r:', label='validation')
        ax.plot(range(epochs), history.loss, 'r-', label='training')
        ax.set_xlabel('Epoche')
        ax.set_ylabel('loss')
        ax = ax.twinx()
        ax.plot(range(epochs), history.val_categorical_accuracy, 'b:', 
                label='validation')
        ax.plot(range(epochs), history.categorical_accuracy, 'b-',
                label='training')
        ax.set_ylabel('Accuracy')
        ax.legend(loc='best')
        plt.tight_layout(pad=0.4)
        plt.savefig(savepath)
        plt.clf()

    def training_RF(self, feature_path, savepath, n_bars=10):
        print('(evaluation) Plot random forest feature importance')
        data = np.load(feature_path + 'feature_importances.npz')
        importances = data['importance']
        std = data['std']
        feature = data['feature']
        indices = np.argsort(importances)[::-1]

        importances = importances[indices][:n_bars]
        std = std[indices][:n_bars]
        feature = feature[indices][:n_bars]

        fig = plt.figure(figsize=(4,3))
        ax = fig.add_subplot(111)
        ax.bar(range(importances.shape[0]), importances,
                       color="r", yerr=std, align="center")
        ax.set_xticks(range(len(feature)))
        ax.set_xticklabels(feature, rotation=45, rotation_mode="anchor", horizontalalignment="right")
        ax.set_xlabel('Bin Grenzen')
        ax.set_ylabel('Feature Importance')
        plt.tight_layout()
        plt.savefig(savepath + 'train_rf.pdf')

    def compare_pre_time(self, models, data_path, testsize=20):
        time_mean = []
        time_std = []
        files = listdir(data_path)[:testsize]
        for model in models:
            time_delta = []
            for filename in files:
                file_path = path.join(data_path,filename)
                tic = time.clock()
                model.predict_photo(file_path)
                toc = time.clock()
                time_delta.append(toc-tic)
            time_mean.append(np.mean(time_delta))
            time_std.append(np.std(time_delta))
        return time_mean, time_std

    def plot_time(self, time_mean, time_std, ticks, save_path):
        fig = plt.figure(figsize=(2.2,2.2))
        ax = fig.add_subplot(111)
        ax.bar(range(len(time_mean)), time_mean,
                       color="r", yerr=time_std, align="center")
        ax.set_xticks(range(len(ticks)))
        ax.set_xticklabels(ticks)
        ax.set_ylabel('Auswertungszeit / s')
        plt.tight_layout(pad=0)
        plt.savefig(save_path + 'time.pdf')
        

def main():
    test_data = np.load('../clouds_per_label/test/hist_dataset.npz')
    rf = Random_forest()
    rf.load('./build/')

    evalu_rf = evaluation(rf, 'Random Forest')
    y_pred = rf.predict(test_data['X'])
    evalu_rf.conf_mat(test_data['y'], y_pred, './build/conf_rf.pdf')
    evalu_rf.training_RF('./build/', './build/')

    net = Network()
    net.load('./build/')

    evalu_net = Evaluation(net, 'CNN')
    evalu_net.training_NN('./build/network.pkl', './build/train_nn.pdf')
    gen = Dataset(shuffle=False)
    train_gen = gen.train_generator('../clouds_per_label/test/')
    y_pred = net.model.predict_generator(train_gen)
    y_pred, y_true = gen.encode_label(train_gen, y_pred)

    evalu_net.conf_mat(y_true, y_pred, './build/conf_nn.pdf')

    time_mean, time_std = evalu_net.compare_pre_time(
            [net, rf], '/home/maxsac/Pictures/preprocessed')
    evalu_net.plot_time(time_mean, time_std, ['CNN','RF'], './build/')

if __name__ == '__main__':
    main()
