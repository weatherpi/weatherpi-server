import logging

RESET = "\033[0m"
BOLD = "\033[1;%dm"
COLOR = "\033[0;%dm"
GREEN = COLOR % 32
YELLOW = COLOR % 33
RED = COLOR % 31
CRITICAL = COLOR % 31


class Logger:
    """Logging class for weatherpi."""

    def __init__(self, name):
        """Create Logger instance."""
        # create logger
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        logger.propagate = False

        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter(
            "[%(asctime)s - %(name)s] %(message)s",
            datefmt='%H:%M:%S'
        )



        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        logger.addHandler(ch)
        self.logger = logger

    def debug(self, msg):
        """Debug message."""
        self.logger.debug(RESET + str(msg) + RESET)

    def info(self, msg):
        """Info message."""
        self.logger.info(GREEN + str(msg) + RESET)

    def warn(self, msg):
        """Warn message."""
        self.logger.warn(YELLOW + str(msg) + RESET)

    def error(self, msg):
        """Error message."""
        self.logger.error(RED + str(msg) + RESET)

    def critical(self, msg):
        """Critical message."""
        self.logger.critical(CRITICAL + str(msg) + RESET)
