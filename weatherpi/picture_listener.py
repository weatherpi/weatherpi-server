import io
import socket
import struct
from PIL import Image, ImageFile
import sys
import os
import weatherpi.weather_db as weather_db
import yaml

from weatherpi.logger import Logger
logger = Logger("Picture Listener")


server_socket = socket.socket()
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('0.0.0.0', 2804))
server_socket.listen(0)

dbm = weather_db.db_manager(
    engine='mysql',
    db_path='weather_pi:password@localhost/weather_data',
)

config_data = yaml.load(open('weatherpi_bot/config.yml', 'r'))
data_path = os.path.expanduser(config_data['data_path'])

class QuitWarn(Exception):
    """Raise warning if connection gets cancelled by user."""


logger.info("Listening ...")
try:
    while True:
        (conn, address) = server_socket.accept()
        try:
            conn.settimeout(1)
            connection = conn.makefile('rb')
            logger.info("Connected ...")

            image_stream = io.BytesIO()
            image_stream.write(connection.read())
            image_stream.seek(0)

            image = Image.open(image_stream)
            name = image_stream.read()[-8:].decode()
            fp = data_path + name + ".png"
            image.save(fp)
            dbm.validate_pic(name.lstrip('0'))
        except socket.timeout as e:
            logger.warn("socket.timeout: {}".format(e))
        except UnicodeDecodeError as e:
            logger.error(e)
        except KeyboardInterrupt:
            logger.info("Closing Connection")
            conn.close()
            raise QuitWarn
        finally:
            conn.close()
except (QuitWarn, KeyboardInterrupt):
    logger.info("Quitting")
    server_socket.close()
    sys.exit(0)
