import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from matplotlib.dates import AutoDateLocator, AutoDateFormatter, HourLocator
import numpy as np
from pendulum import now
from weatherpi.weather_db import db_manager

mpl.rcParams["date.autoformatter.year"] = "%Y"
mpl.rcParams["date.autoformatter.month"] = "%m.%Y"
mpl.rcParams["date.autoformatter.day"] = "%d.%m.%Y"
mpl.rcParams["date.autoformatter.hour"] = "%d.%m %H:%M"
mpl.rcParams["date.autoformatter.minute"] = "%d. %H:%M"
mpl.rcParams["date.autoformatter.second"] = "%H:%M:%S"
mpl.rcParams["date.autoformatter.microsecond"] = "%M:%S.%f"
mpl.rcParams["axes.grid"] = True
mpl.rcParams["axes.spines.right"] = False
mpl.rcParams["axes.spines.top"] = False
mpl.rcParams["axes.spines.left"] = False
mpl.rcParams["axes.spines.bottom"] = False


class Plotter:
    def __init__(self, dim=111):
        self.fig = plt.figure(figsize=(1.61 * 5, 5), dpi=100)
        self.dim = dim
        self.ax = self.fig.add_subplot(self.dim)
        self.fig.patch.set_facecolor("white")
        self.fig.patch.set_alpha(0)

    def add_temp(self, temp, label="temperatur"):
        self.ax.yaxis.set_major_formatter(FormatStrFormatter("%.1f"))
        self.ax.plot(self.x_axis, temp, "x--", label=label)
        self.ax.set_ylabel("Temperature / °C")

    def add_hum(self, hum, label="humidity"):
        self.ax.plot(self.x_axis, hum, "g--", label=label)
        self.ax.set_ylabel("Humidity / %")

    def add_press(self, press, label="pressure"):
        self.ax.plot(self.x_axis, press, "r--", label=label)
        self.ax.set_ylabel("Pressure / hPa")

    def add_xaxis(self, time):
        self.x_axis = time

    def add_sub_plot(self, dim=None):
        if dim == None:
            self.dim = self.dim + 1
        else:
            self.dim = dim
        self.ax = self.fig.add_subplot(self.dim)

    def add_twinx_plot(self):
        self.ax = self.ax.twinx()
        self.ax.grid(False)

    def save(self, name):
        self.ax.legend(loc="best")
        self.fig.autofmt_xdate()
        self.fig.savefig(
            name,
            facecolor=self.fig.get_facecolor(),
            edgecolor="none",
            transparent=True,
        )
        plt.close(self.fig)


def main():
    dbm = db_manager(db_path="weatherpi.db")
    dbm.login_owner("Maximilian")
    dbm.add_weather(22.5, 22.6, 45, 994)
    data = dbm.get_weather(
        ["temperature_1", "temperature_2", "humidity", "pressure", "time"],
        time=now().subtract(days=5),
    )

    plotter = Plotter(211)
    plotter.add_xaxis(data["time"])
    plotter.add_temp(data["temperature_1"], "temp 1")
    plotter.add_temp(data["temperature_2"], "temp 2")
    plotter.add_sub_plot()
    plotter.add_press(data["humidity"], "Dortmund")
    plotter.add_twinx_plot()
    plotter.add_hum(data["pressure"], "Dortmund")
    plotter.save("test.pdf")


if __name__ == "__main__":
    main()
