import sqlalchemy
from sqlalchemy import (
    create_engine,
    Column,
    Float,
    Integer,
    String,
    Boolean,
    ForeignKey,
    DateTime,
    func,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from datetime import datetime

import sys

from weatherpi.logger import Logger
logger = Logger("DB Manager")

Base = declarative_base()

class Weatherpi(Base):
    __tablename__ = "weatherpi"
    id = Column(Integer, primary_key=True)
    owner = Column(String(32))
    location = Column(String(32))
    weather_data = relationship("Weather_data", backref="weatherpi")
    # weather_pic = relationship("Weather_pic", backref="weatherpi")

class Weather_data(Base):
    __tablename__ = "weather_data"
    id = Column(Integer, primary_key=True)
    pi_id = Column(Integer, ForeignKey("weatherpi.id"))
    temperature_1 = Column(Float)
    temperature_2 = Column(Float)
    humidity = Column(Float)
    pressure = Column(Float)
    picture = Column(Boolean)
    time = Column(DateTime(timezone=True))
    weather_pic = relationship("Weather_pic", backref="weather_data")


class Weather_pic(Base):
    __tablename__ = "weather_pic"
    id = Column(Integer, primary_key=True)
    data_id = Column(Integer, ForeignKey("weather_data.id"))
    cloud_class = Column(String(32))


class db_manager:
    def __init__(self, db_path="/production/weather.db", engine="mysql", pp_ping=True):
        engine = create_engine("{}://{}".format(engine, db_path), pool_pre_ping=pp_ping)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        try:
            Base.metadata.create_all(engine)
            self.session.commit()
        except sqlalchemy.exc.OperationalError as e:
            logger.error(e)
            logger.warn("SQL Server not running. Start with `sudo systemctl start mariadb.service`")
            sys.exit(0)

    def add_pic(self, data_id):
        wp = Weather_pic(
                data_id = data_id,
                cloud_class = 'not labeled',
                )
        self.session.add(wp)
        self.session.commit()
    
    def search_owner(self, name):
        wp_owner = self.session.query(Weatherpi).filter(
                Weatherpi.owner==name).one()
        return wp_owner.id
    
    def search_location(self, loc):
        wp_owner = self.session.query(Weatherpi).filter(
                Weatherpi.location==loc).one()
        return wp_owner.id

    def search_weather(self, params, user="all", time=None):
        Params = [*params, 'pi_id', 'time'] # add time and pi_id for searching
        sql_params = [getattr(Weather_data, x) for x in Params]
        self.session.commit()
        if(user == 'all'):
            wd = self.session.query(*sql_params)
        else:
            wd = self.session.query(*sql_params).filter(
                 Weather_data.pi_id == user
                 )
        if(time == None):
            time = wd.order_by(Weather_data.time)[-1].time
        wd = wd.filter(Weather_data.time >= time)
        return wd

    def get_weather(self, params, user="all", time=None):
        wd = self.search_weather(params, user=user, time=time)
        Data = {}
        for param in params:
            Data[param] = [getattr(x, param) for x in wd]
        return Data

    def get_locations(self):
        self.session.commit()
        wp_loc = self.session.query(Weatherpi.location).all()
        return [[x.location] for x in wp_loc]

    def validate_pic(self, data_id):
        wd = (self.session.query(Weather_data).filter(
            Weather_data.id == data_id
        ).one())
        self.add_pic(data_id)
        wd.picture = True
        self.session.commit()
        logger.info('Verified picture {}'.format(data_id))

def main():
    dbm = db_manager(engine= 'mysql', db_path='weather_pi:password@localhost/weather_data')
    # dbm.add_weather(20,20,75,1000)
    # dbm.add_pic(3)
    # dbm.validate_pic(60)
    # lo

if __name__ == '__main__':
    main()
