from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
import numpy as np
import yaml


class Keyboards:
    """Keyboards."""

    def __init__(self, config_file):
        """Initialize all keyboards using configurations from config file."""
        self.config_data = yaml.load(open(config_file))
        self.classes = self.config_data["classes"]

        self.next_keyboard = [["next"]]
        self.next_markup = ReplyKeyboardMarkup(
            self.next_keyboard, one_time_keyboard=True
        )
        self.next_markup = ReplyKeyboardMarkup([["next"]])

        self.main_menu_keyboard = [
            ["label", "weather info"],
            ["something else..."],
        ]
        self.main_menu_markup = ReplyKeyboardMarkup(self.main_menu_keyboard)
        self.main_menu_markup = ReplyKeyboardMarkup(
            [["label", "weather info"],
             ["predict", "more"]],
        )

        self.label_keyboard = self.reshape_list_fitted(
            self.classes + ["info", "back"]
        )
        self.label_markup = ReplyKeyboardMarkup(self.label_keyboard)

        self.info_keyboard = self.reshape_list_fitted(
            self.classes + ["general"]
        )
        self.info_markup = ReplyKeyboardMarkup(self.info_keyboard)

        self.back_or_info_keyboard = [["back", "info"]]
        self.back_or_info_markup = ReplyKeyboardMarkup(
            self.back_or_info_keyboard
        )
        self.back_or_info_markup = ReplyKeyboardMarkup(
            [["back", "info"]]
        )

        self.next_or_main_keyboard = [["main", "next"]]
        self.next_or_main_markup = ReplyKeyboardMarkup(
            self.next_or_main_keyboard
        )
        self.next_or_main_markup = ReplyKeyboardMarkup(
            [["main", "next"]]
        )

        self.label_menu_keyboard = [["label", "check"]]
        self.label_menu_markup = ReplyKeyboardMarkup(
            self.label_menu_keyboard
        )
        self.label_menu_markup = ReplyKeyboardMarkup(
            [["label", "check"]]
        )

        self.classes_keyboard = self.reshape_list_fitted(self.classes)

        self.classes_markup = ReplyKeyboardMarkup(self.classes_keyboard)

        self.bool_keyboard = [["Stimmt", "Stimmt nicht"]]
        self.bool_markup = ReplyKeyboardMarkup(self.bool_keyboard)

        self.remove_markup = ReplyKeyboardRemove()

    def generate_keyboard_markup(self, l):
        """Return a freshly generated keyboardmarkup."""
        return ReplyKeyboardMarkup(l)

    def reshape_list_to_square_matrix(self, l):
        """Reshape list to square matrix.

        If len(l) is not squarerootable, fill l until it is.
        Change type to array and reshape it to square matrix.

        Parameters
        ----------
        l: list
            List to be squared.

        Returns
        -------
        np.array
            Squared matrix.

        Examples
        --------
        >>> l = [1, 2, 3, 4, 'a', 'b', 'c']
        >>> reshape_list_to_square_matrix(l)
        array([['1', '2', '3'],
               ['4', 'a', 'b'],
               ['c', '', '']], dtype='<U21')

        """
        for i in range(int(np.ceil(np.sqrt(len(l))) ** 2 - len(l))):
            l.append("")
        a = np.array(l)
        return a.reshape((int(np.sqrt(len(a))), int(np.sqrt(len(a)))))

    def reshape_list_fitted(self, l):
        """Reshape list to fit specified columns.

        Parameters
        ----------
        l: list
            List to be squared.

        Returns
        -------
        np.array
            Reshaped matrix.

        """
        cols = self.config_data["keyboard"]["columns"]
        if cols == 0:
            return self.reshape_list_to_square_matrix(l)
        else:
            rows = int(np.ceil(len(l) / cols))
        if len(l) == (rows * cols):
            a = np.array(l)
        else:  # Fill list with empty strings
            for _ in range(np.abs(rows * cols - len(l))):
                l.append("")
            a = np.array(l)
        return a.reshape((rows, cols))
