from weatherpi.weatherpi_bot.keyboards import Keyboards
import logging
import time
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    RegexHandler,
    ConversationHandler,
)
from telegram import ChatAction
from telegram.error import BadRequest
import weatherpi.weatherpi_bot.label_handler as lab_han

import weatherpi.weather_db as db_manager
import weatherpi.weatherpi_bot.plot as plotter
import os
from weatherpi.logger import Logger

from predictions.models.neuralnet import Network
# from predictions.models.randomforest import Random_forest


logging.basicConfig(
    format="[%(asctime)s - %(name)s] %(message)s",
    datefmt="%H:%M:%S",
    level=logging.INFO,
)

logger = Logger("Botfather")

CHOOSING = 1
MAIN_MENU = 2
LABEL_MENU = 3
LABEL_REPLY = 4
LABEL_CHECK = 5
SEND_CHECK = 6
SEND_INFO = 7
INFO_HANDLER = 8
PAST_LABEL = 9
PAST_CHECK = 10
WEATHER_MENU = 11
MORE = 12
PREDICT = 13
PREDICT_PHOTO = 14
POST_PREDICTION = 15


class Botfather(Keyboards):
    """Botfather."""

    def __init__(self, configfile):
        """Start Botfather, which holds whole functionality of the Telegram UI.

        Initialize
        - The process dict, in which information about the users are stored, such as the msg id of the last sent picture, how many pictures were labeled and how fast.
        - The label_handler works with the dataframe which stores the labels and the corresponding filenames.
        - The plotter plots progress
        - The db_handler works with the sql database and read weather data
        """
        super().__init__(configfile)
        self.process_dict = {}
        self.label_handler = lab_han.label_handler(configfile)
        self.label_handler.check_predictions()
        self.plotter = plotter.bot_plotter(self.label_handler)
        self.db_handler = db_manager.db_manager(engine='mysql', db_path="weather_pi:password@localhost/weather_data")
        self.nn = Network()
        self.nn.load(os.path.expanduser("~/Documents/weatherpi-server/predictions/pipeline/build/"))


    def start(self, bot, update):
        """Message called upon starting the bot."""
        update.message.reply_markdown(
            "*Herzlich Wilkommen beim weatherpi Projekt!*\n\n"
            "Dieses Projekt entsteht im Rahmen einer Forschungsarbeit an der "
            "TU Dortmund. Es dient der Frage, ob sich mit naiven Mitteln und "
            "einem begrenztem Budget Wetterdaten mit Hilfe maschineller "
            "Methoden auswerten lassen.\n\n"
            "Es besteht die Moeglickeit einerseits, die mit der "
            "Wetterstation aufgenommen Daten auszuwerten, als auch den "
            "Trainingsdatensatz, auf dem die Algorithmen optimiert werden, zu "
            "verbessern.\n\n"
            "Die Bedienung erfolgt uber das Keyboard unter der Eingabezeile. "
            "Wir wunschen ihnen viel Spass",
            reply_markup=self.next_markup,
        )
        # Add user to process_dict and add default values for fields
        self.process_dict[update.message.chat_id] = {'last_pic': None}
        return MAIN_MENU

    def main_menu(self, bot, update):
        """Dostring."""
        update.message.reply_markdown(
            "*Hauptmenu*\n"
            "Wahlen Sie einen Menupunkt, zu dem sie sich bewegen wollen:\n\n"
            "_label:_ Prozessierung eines Datensatzes zum "
            "Trainieren von Algorithmen und Informationen uber die verschiedenen Wolkentypen\n\n"
            "_weather info:_ Informationen uber das aktuelle Wetter sowie Wettervorhersagen.\n\n"
            "_more_: Informationen, Anleitungen, Fortschrittsanzeige und mehr",
            reply_markup=self.main_menu_markup,
            action=ChatAction.TYPING,
        )
        return CHOOSING

    def weather_menu(self, bot, update):
        """Dostring."""
        location = update.message.text
        pi_id = self.db_handler.search_location(location)
        data = self.db_handler.get_weather(
            user=pi_id, params=["id", "temperature_1", "temperature_2", "humidity", "pressure", "picture", "time"]
        )
        if data['picture'][0]:
            pic_name = str(data['id'][0]).zfill(8) + '.png'
            self.delete_old_and_send_new_photo(
                bot,
                update.message.chat_id,
                os.path.expanduser(self.config_data['data_path']) + pic_name,
            )
        update.message.reply_markdown(
            "*Wettermenu* ({})\n"
            "Die Temperatur in {} betraegt *{} C*, bei einem Luftdruck von "
            "*{} mBar* und einer Luftfeuchtigkeit von *{}%*.".format(
                *data["time"],
                location,
                *data["temperature_2"],
                *data["pressure"],
                *data["humidity"]
            ),
            reply_markup=self.next_markup,
            action=ChatAction.TYPING,
        )

        return MAIN_MENU


    def choose_location(self, bot, update):
        """Docstring."""
        stations = self.db_handler.get_locations()
        update.message.reply_markdown(
            "Waehlen sie aus, mit welcher Wetterstation sie interagieren moechten.""",
            reply_markup=self.generate_keyboard_markup(stations),
            action=ChatAction.TYPING,
        )
        return WEATHER_MENU
        
        

    def info_menu(self, bot, update):
        """Dostring."""
        update.message.reply_markdown(
            "*Infomenu* \n"
            "Wahlen Sie den Wolkentyp auss, uber den Sie weiter Informationen"
            "erhalten wollen. Wahlen Sie den Punkt Cloudsheet "
            "aus, um einen Ubersicht uber alle Wolkentypen zu erhalten.",
            reply_markup=self.info_markup,
            action=ChatAction.TYPING,
        )
        return SEND_INFO


    def label_menu(self, bot, update):
        """Dostring."""
        update.message.reply_markdown(
            "*Labelmenu* \n"
            "Wahlen Sie aus ob Sie neue Wolken klassifizieren wollen, "
            "oder die Label der Bereits vorhergesagten ueberpruefen "
            "wollen",
            reply_markup=self.label_menu_markup,
            action=ChatAction.TYPING,
        )
        self.process_dict[update.message.chat_id]["counter"] = 0
        self.process_dict[update.message.chat_id]["time"] = time.time()
        return LABEL_MENU

    def check_label(self, bot, update):
        """Dostring."""
        if self.label_handler.check_need_to_label():
            update.message.reply_markdown(
                "*Checkmenu* \n"
                "Waehlen Sie die Klasse aus welche sie ueberpruefen wollen",
                reply_markup=self.classes_markup,
                action=ChatAction.TYPING,
            )
            self.process_dict[update.message.chat_id]["class_set"] = False
            return SEND_CHECK
        else:
            update.message.reply_markdown(
                "Alles ist fertig uberprueft, Sie kehren ins Hauptmenu "
                "zuruck.",
                reply_markup=self.next_markup,
            )
            return MAIN_MENU

    def send_check(self, bot, update):
        """Dostring."""
        if self.process_dict[update.message.chat_id]["class_set"] == False:
            self.process_dict[update.message.chat_id][
                "last_label"
            ] = update.message.text
            self.process_dict[update.message.chat_id]["class_set"] = True
        label = self.process_dict[update.message.chat_id]["last_label"]

        try:
            picture_path = self.label_handler.get_pic_to_check(
                update.message.chat_id, label
            )
            update.message.reply_markdown(
                "Entspricht das Photo dem Label {}".format(label),
                reply_markup=self.bool_markup,
                action=ChatAction.TYPING,
            )
            self.delete_old_and_send_new_photo(
                bot,
                update.message.chat_id,
                picture_path,
            )
            return LABEL_CHECK
        except ValueError:
            update.message.reply_markdown(
                "Die Klasse {} wurde fertig uberprueft, Sie kehren ins Hauptmenu "
                "zuruck.".format(label),
                reply_markup=self.next_markup,
            )
            return MAIN_MENU

    def label_check(self, bot, update):
        """Dostring."""
        bool_resp = update.message.text
        self.label_handler.check_pic(update.message.chat_id, bool_resp)
        update.message.reply_markdown(
            "Sie haben das Foto als {} bestaetigt.".format(bool_resp),
            reply_markup=self.next_or_main_markup,
            action=ChatAction.TYPING,
        )
        bot.delete_message(
            update.message.chat_id,
            self.process_dict[update.message.chat_id]["last_pic"],
        )
        self.process_dict[update.message.chat_id]["counter"] += 1
        return PAST_CHECK

    def motivation_feedback(self, bot, update):
        """Dostring."""
        counter = self.process_dict[update.message.chat_id]["counter"]
        t_delta = time.time() - self.process_dict[update.message.chat_id]["time"]
        update.message.reply_markdown(
            "Sie haben {} Fotos mit einer Rate von {:.2f} Hz gelabelt.\n *Vielen Dank!*".format(
                counter, counter / t_delta
            ),
            action=ChatAction.TYPING,
            reply_markup=self.next_markup,
        )
        return MAIN_MENU

    ##################################################################

    def info_handler(self, bot, update):
        """Dostring."""
        update.message.reply_markdown(
            "*Info Handler* \n",
            reply_markup=self.back_or_info_markup,
            action=ChatAction.TYPING,
        )
        return SEND_INFO

    def more_menu(self, bot, update):
        text = (
            "_progress_: Fortschrittsanzeige fur das Labeln der Fotos\n"
            "_information_: Informationen uber dieses Projekt"
        )
        update.message.reply_markdown(
            text,
            reply_markup=self.generate_keyboard_markup(
            [["progress", "information"]],
            ),
            action=ChatAction.TYPING,
        )
        return MORE

    def predict_menu(self, bot, update):
        text = (
            "*Vorhersage*\n"
            "Senden Sie dem Bot ein Wolkenfoto, damit dieses klassifiziert "
            "werden kann. Unter dem Menuepunkt der Bueroklammer in der  "
            "Texteingabe, laesst sich in Telegram ein Foto versenden."
       )
        update.message.reply_markdown(
            text,
            reply_markup=self.generate_keyboard_markup(
            [["back"]],
            ),
            action=ChatAction.TYPING,
        )
        return PREDICT_PHOTO

    def predict_photo(self, bot, update):
        pic = update.message.photo[-1]
        pic_file = pic.get_file()
        pic_file.download(custom_path="/tmp/download", out=None)
        cls, prob = self.nn.predict_photo("/tmp/download")
        text = (
            "Die Wolkenklasse ist *{}* mit einer Wahrscheinlichkeit von *{}%* .".format(
              cls, round(100*prob, 3))
        )
        self.process_dict[update.message.chat_id]["class_pic"] = cls
        update.message.reply_markdown(
            text,
            reply_markup=self.generate_keyboard_markup(
            [["next", "info"]],
            ),
            action=ChatAction.TYPING,
        )
        return POST_PREDICTION

    def information(self, bot, update):
        text = (
            "*Informationen*\n"
            "_Website:_ twitter.com/WPiProject\n"
            "_Autoren:_ Maxi, Noah"
        )
        update.message.reply_markdown(
            text,
            reply_markup=self.next_markup,
            action=ChatAction.TYPING,
        )
        return MAIN_MENU

    def progress(self, bot, update):
        """Dostring."""
        progress, counts = self.plotter.hist_label()
        if progress == 100.0:
            motivation = "Done! Congratulations!"
        elif progress > 75.0:
            motivation = "Only {} left, keep going!".format(counts)
        else:
            motivation = "Still much work to do, better start now."
        update.message.reply_markdown(
            motivation,
            reply_markup=self.next_markup,
            action=ChatAction.TYPING,
        )
        self.delete_old_and_send_new_photo(
            bot,
            update.message.chat_id,
            "label.png",
        )
        return MAIN_MENU

    def send_img(self, bot, update):
        """Dostring."""
        msg_id_old_photo = self.process_dict[update.message.chat_id]["last_pic"]
        # Delete old photo if old photo is not last labeled photo (because then it was already deleted)
        if msg_id_old_photo is not None:
            bot.delete_message(update.message.chat_id, msg_id_old_photo)

        try:
            path = self.label_handler.get_pic_to_label(update.message.chat_id)
            update.message.reply_markdown(
                "*Labelmenu*\n"
                "Bitte labeln Sie das gesendete Foto mit den "
                "entsprechenden Auswahlmoglichkeitne. Benotigen Sie weitere "
                "Informationen, weil Sie sich nicht sicher sind, zu welcher "
                "Klasse der Wolkentyp gehort, klicken Sie auf _info_.",
                reply_markup=self.label_markup,
            )
            sent_photo_infos = bot.send_photo(
                update.message.chat_id,
                photo=open(path, "rb"),
                action=ChatAction.UPLOAD_PHOTO,
            )
            self.process_dict[update.message.chat_id]["last_pic"] = sent_photo_infos["message_id"]

            return LABEL_REPLY
        except ValueError:
            update.message.reply_markdown(
                "Alles ist fertig gelabelt, Sie kehren ins Hauptmenu zuruck.",
                reply_markup=self.next_markup,
            )
            return MAIN_MENU

    def last_img(self, bot, update):
        """Dostring."""
        update.message.reply_markdown(
            "*Labelmenu*\n"
            "Bitte labeln Sie das gesendete Foto mit den "
            "entsprechenden Auswahlmoglichkeitne. Benotigen Sie weitere "
            "Informationen, weil Sie sich nicht sicher sind, zu welcher "
            "Klasse der Wolkentyp gehort, klicken Sie auf _info_.",
            reply_markup=self.label_markup,
            reply_to_message_id=self.process_dict[update.message.chat_id][
                "last_pic"
            ],
            action=ChatAction.TYPING,
        )
        return LABEL_REPLY

    def label_data(self, bot, update):
        """Dostring."""
        text = update.message.text
        self.label_handler.set_label(text, update.message.chat_id)
        update.message.reply_text(
            "Sie labeln das Foto '{}'.\nVielen Dank.".format(text.lower()),
            reply_markup=self.next_or_main_markup,
            action=ChatAction.TYPING,
        )
        bot.delete_message(
            update.message.chat_id,
            self.process_dict[update.message.chat_id]["last_pic"],
        )
        # Reset last picture msg_id, because it was deleted
        self.process_dict[update.message.chat_id]["last_pic"] = None
        self.process_dict[update.message.chat_id]["counter"] += 1
        return PAST_LABEL

    def get_info(self, bot, update):
        cls = self.process_dict[update.message.chat_id]["class_pic"]
        filename = "../../documents/clouds/build/{}-1.png".format(cls)
        self.delete_old_and_send_new_photo(
            bot,
            update.message.chat_id,
            filename,
            reply_markup=self.next_markup,
        )
        return MAIN_MENU
        

    def send_info(self, bot, update):
        """Dostring."""
        text = update.message.text
        filename = "../../documents/clouds/build/{}-1.png".format(text)
        self.delete_old_and_send_new_photo(
            bot,
            update.message.chat_id,
            filename,
        )
        update.message.reply_markdown(
            "*Labelmenu*\n"
            "_back:_ Zuruck zum zu labelnden Foto.\n\n"
            "_info:_ Weitere Informationen zu den Wolkentypen.\n\n",
            reply_markup=self.back_or_info_markup,
            action=ChatAction.TYPING,
        )
        return INFO_HANDLER


    def delete_old_and_send_new_photo(self, bot, chat_id, path_to_photo, reply_markup=""):
        msg_id_old_photo = self.process_dict[chat_id]["last_pic"]

        if msg_id_old_photo is not None:
            bot.delete_message(chat_id, msg_id_old_photo)

        sent_photo_infos = bot.send_photo(
            chat_id,
            photo=open(path_to_photo, "rb"),
            reply_markup=reply_markup,
            action=ChatAction.UPLOAD_PHOTO,
        )

        # Update self.process_dict
        self.process_dict[chat_id][
            "last_pic"
        ] = sent_photo_infos["message_id"]


    def cancel(self, bot, update):
        """Message called upon stopping the bot."""
        update.message.reply_text(
            "Konversation ist zu Ende!",
            reply_markup=self.remove_markup,
            action=ChatAction.TYPING,
        )
        return ConversationHandler.END


def main():
    """Run the bot."""
    updater = Updater(token="501440002:AAG2CGDPb3GwWhSzAeZJp8GohBxDcNMuXjw")
    configfile = "./config.yml"

    # updater = Updater(token="475494777:AAFxuvbrh_zb2DGrlx0RYoi9b2XJoDWgKNE")
    # configfile = "./example/config.yml"

    botfather = Botfather(configfile)

    conversation_handler = ConversationHandler(
        entry_points=[CommandHandler("start", botfather.start)],
        states={
            CHOOSING: [
                RegexHandler("^(label)$", botfather.label_menu),
                RegexHandler("^(weather info)$", botfather.choose_location),
                RegexHandler("^more$", botfather.more_menu),
                RegexHandler("^predict$", botfather.predict_menu),
            ],
            MORE: [
                RegexHandler("^(progress)$", botfather.progress),
                RegexHandler("^(information)$", botfather.information),
            ],
            PREDICT_PHOTO: [
                MessageHandler(Filters.photo, botfather.predict_photo),
                MessageHandler(Filters.text, botfather.main_menu),
            ],
            POST_PREDICTION: [
                RegexHandler("^(next)$", botfather.main_menu),
                RegexHandler("^(info)$", botfather.get_info),
            ],
            MAIN_MENU: [MessageHandler(Filters.text, botfather.main_menu)],
            LABEL_MENU: [
                RegexHandler("^(label)$", botfather.send_img),
                RegexHandler("^(check)$", botfather.check_label),
            ],
            LABEL_CHECK: [MessageHandler(Filters.text, botfather.label_check)],
            SEND_CHECK: [MessageHandler(Filters.text, botfather.send_check)],
            LABEL_REPLY: [
                RegexHandler(
                    "^({})$".format("|".join(botfather.classes)),
                    botfather.label_data,
                ),
                RegexHandler("^(info)$", botfather.info_menu),
                RegexHandler("^(back)$", botfather.main_menu),
            ],
            PAST_LABEL: [
                RegexHandler("^(next)$", botfather.send_img),
                RegexHandler("^(main)$", botfather.motivation_feedback),
            ],
            PAST_CHECK: [
                RegexHandler("^(next)$", botfather.send_check),
                RegexHandler("^(main)$", botfather.motivation_feedback),
            ],
            SEND_INFO: [MessageHandler(Filters.text, botfather.send_info)],
            INFO_HANDLER: [
                RegexHandler("^(back)$", botfather.last_img),
                RegexHandler("^(info)$", botfather.info_menu),
            ],
            WEATHER_MENU: [MessageHandler(Filters.text, botfather.weather_menu)],
        },
        fallbacks=[CommandHandler("cancel", botfather.cancel)],
    )

    updater.dispatcher.add_handler(conversation_handler)
    logger.info("Listening ...")
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
