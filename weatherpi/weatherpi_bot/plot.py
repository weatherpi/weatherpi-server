import numpy as np
import pandas as pd
import matplotlib

matplotlib.use("agg")
import matplotlib.pyplot as plt
from weatherpi.weatherpi_bot import label_handler

plt.style.use("fivethirtyeight")


class bot_plotter:
    def __init__(self, lh=None):
        if lh == None:
            self.label_handler = label_handler.label_handler("./config.yml")
        else:
            self.label_handler = lh
        self.clouds = self.label_handler.classes

    def hist_label(self):
        label = self.label_handler.get_final_label()
        self.label_handler.update_filename()
        counts = []
        for cloud in self.clouds:
            counts.append(np.sum(label == cloud))
        n_label = np.sum(counts)
        n_data = len(self.label_handler.df.index)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        progress = 100 * n_label / n_data
        ax.set_title("{0:.2f}% are labeled".format(progress))
        ax.bar(self.clouds, counts)
        # for a, b, in zip(self.clouds, counts):
        # ax.text(a, 1.1 * b, b)
        for tick in ax.get_xticklabels():
            tick.set_rotation(90)
        ax.set_yscale("log")
        plt.tight_layout()
        plt.savefig("label.png")
        plt.close()
        return progress, n_data - n_label


def main():
    pltter = bot_plotter()
    pltter.hist_label()


if __name__ == "__main__":
    main()
