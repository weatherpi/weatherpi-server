import pandas as pd
import numpy as np
import os
import pendulum
import random
import yaml

from weatherpi.logger import Logger
logger = Logger("LabelHandler")


class label_handler:
    def __init__(self, config_file):
        config_data = yaml.load(open(config_file))
        
        self.last_update = pendulum.datetime(1879, 3, 14)
        self.last_filename = dict()

        self.classes = np.array(config_data["classes"])
        self.data_path = os.path.expanduser(config_data["data_path"])
        self.label_path = os.path.expanduser(config_data["label_path"])
        self.classified_label = os.path.expanduser(config_data["classified_label"])
        self.threshold = config_data["threshold"]

        if os.path.isfile(self.label_path):
            logger.info(
                "A labelfile already exist. It will"
                "be loaded and appended."
            )
            self.df = pd.read_pickle(self.label_path)
        else:
            logger.warn("A new label file will be created.")
            self.df = pd.DataFrame(
                columns=["filename", *self.classes, "final_label"]
            )

    def save_label(self):
        self.df.to_pickle(self.label_path)

    def update_filename(self, hours=3):
        if pendulum.now().subtract(hours=hours) > self.last_update:
          logger.info("Filenames have been updated.")
          dff = pd.DataFrame({"filename": os.listdir(self.data_path)})
          self.df = self.df.merge(
              dff, on="filename", how="outer", validate="one_to_one"
          )
          self.df[self.classes] = self.df[self.classes].fillna(0)
          self.last_update = pendulum.now()

    def check_need_to_label(self):
        # if nan in final_labels => at least one picture has no final label
        # returns true if need to label at least one picture
        logger.warn('Please check function `check_need_to_label`')
        return "not checked" in self.df_classified['status']

    def get_pic_to_label(self, user):
        self.update_filename()
        mask = self.df.sum(axis=1) < self.threshold
        if sum(mask) == 0:
            logger.info(
                "Everything labeled, be happy that"
                "work is done or add more data"
            )
        self.last_filename[user] = np.random.choice(self.df.filename[mask])
        return self.data_path + self.last_filename[user]

    def set_label(self, label, user):
        filename = self.last_filename[user]
        mask = filename == self.df["filename"]
        self.df.loc[mask, label] += 1
        self.save_label()

    def get_final_label(self):
        # logger.info("Generated Final label.")
        # logger.warn(' final label generator have to be checked!')
        mask = self.df.loc[:, self.classes].sum(axis=1) >= self.threshold
        final_label = self.df.loc[mask, self.classes].values.argmax(axis=1)
        self.df.loc[mask, "final_label"] = self.classes[final_label]
        self.save_label()
        return self.df.final_label

    def check_predictions(self):
        self.df_classified = pd.read_pickle(self.classified_label)
        self.df_classified["status"] = "not checked"

    def save_predictions(self):
        self.df_classified.to_pickle(self.classified_label)

    def get_pic_to_check(self, user, label=None):
        mask = self.df_classified.status == "not checked"
        self.df_classified = self.df_classified[mask]
        # logger.warn('(Label handler) Diese Funktion muss noch vielen weiteren Tests '
        #     'unterzogen werden. Die Funktion mit dem Label auswaehlen scheint '
        #     'noch nicht zu funkitonieren.')
        if label != None:
            dff = self.df[self.df.final_label == label]
            mask_not_checked = np.in1d(
                dff.filename, self.df_classified.filename
            )
        if np.sum(mask) == 0:
            logger.info(
                "Everything checked, be happy that "
                "work is done or add more data"
            )
        self.last_filename[user] = np.random.choice(
            dff.loc[mask_not_checked, "filename"]
        )
        return self.data_path + self.last_filename[user]

    def check_pic(self, user, bool_resp):
        pos = self.df.filename == self.last_filename[user]
        bool_encoder = {"Stimmt": True, "Stimmt nicht": False}
        if bool_encoder[bool_resp]:
            logger.info("Label accepted", self.last_filename[user])
            # logger.warn(' Risky because not checked.')
            label = self.df.loc[pos, "final_label"]
            self.df.loc[pos, label] += 1
        else:
            logger.info("Label denied ", self.last_filename[user])
            self.df.loc[pos, self.classes] = 0
            self.df.loc[pos, "final_label"] = None
        pos = self.df_classified.filename == self.last_filename[user]
        self.df_classified.loc[pos, "status"] = "checked"
        self.save_label()
        self.save_predictions()


def main():
    handler = label_handler("./config.yml")
    # pic = handler.get_pic_to_label('maximilian')
    # handler.set_label('altocumulus', 'maximilian')
    handler.get_final_label()
    # handler.check_predictions()
    # handler.get_pic_to_check('maximilian')
    # handler.check_pic('maximilian','altocumulus')


if __name__ == "__main__":
    main()
