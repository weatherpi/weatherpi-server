import logging
from telegram.ext import (
    Updater,
    CommandHandler,
    RegexHandler,
    ConversationHandler,
)
from telegram import ChatAction, ReplyKeyboardMarkup, ReplyKeyboardRemove

# import weatherpi.weather_db as db_manager
from weatherpi.weatherstation import weatherstation
from weatherpi.sensor.camera_sens import camera_sens
from os import remove

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)

AUSWAHL = 0

main_menu_keyboard = [["Wetter"], ["Foto"]]
main_menu_markup = ReplyKeyboardMarkup(main_menu_keyboard)


def start(bot, update):
    update.message.reply_markdown(
        "*Hallo.*\nIch bin ein kleiner WeatherPi-Bot. Ich schicke dir aktuelle Wetterdaten und Fotos.",
        reply_markup=main_menu_markup,
    )
    return AUSWAHL


def wetter(bot, update):
    t1, t2, hum, press = wst.load_data()
    update.message.reply_markdown(
        "Es sind {}C, bei einer Luftfeuchtigkeit von {}% und einem Luftdruck von {}mBar.".format(
            (t1 + t2) / 2.0, hum, press
        ),
        action=ChatAction.TYPING,
    )
    return AUSWAHL


def foto(bot, update):
    pic = cam.get_picture(name="/tmp/temp.jpg")
    if pic:
        bot.send_photo(
            update.message.chat_id,
            photo=open("/tmp/temp.jpg", "rb"),
            action=ChatAction.UPLOAD_PHOTO,
        )
        remove("/tmp/temp.jpg")
    else:
        update.message.reply_markdown(
            "Es ist zu dunkel, also wurde kein Foto aufgenommen.",
            action=ChatAction.TYPING,
        )
    return AUSWAHL


def cancel(bot, update):
    update.message.reply_text(
        "Mach's gut!",
        reply_markup=ReplyKeyboardRemove(),
        action=ChatAction.TYPING,
    )
    return ConversationHandler.END


def main():
    with open("/home/pi/.smallbot_token", "r") as f:
        token = f.readline().strip()
    print(token)
    updater = Updater(token=token)

    # global db_handler
    # db_handler = db_manager.db_manager("")

    global cam
    cam = camera_sens()

    global wst
    wst = weatherstation()

    dp = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            AUSWAHL: [
                RegexHandler("^Wetter$", wetter),
                RegexHandler("^Foto$", foto),
            ]
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    dp.add_handler(conv_handler)
    print("listening...")
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
