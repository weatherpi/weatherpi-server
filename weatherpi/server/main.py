from flask import Flask, render_template, Markup
from weatherpi.data.now import get_now, plot_now
from gpiozero import LED


app = Flask(__name__)
led = LED(17)


@app.route("/index")
def index():
    led.on()
    content = Markup(
        "<h1>Links</h1>"
        '<p><a href="/index">/index</a></p>'
        '<p><a href="/">/</a></p>'
        '<p><a href="/now">/now</a></p>'
        '<p><a href="/data">/data</a></p>'
        '<p><a href="/forecast">/forecast</a></p>'
    )
    led.off()
    return render_template("about.html", content=content)


@app.route("/")
def about():
    led.on()
    about_text = Markup(
        "<p>This page is intended to serve a webinterface for a weatherstation running on several Raspberry Pis.</p>"
        "<p>It runs itself on a Raspberry Pi.</p>"
        '<p>Get current weather <a href="/now">here</a>.</p>'
        '<p>Go to <a href="/index">link list</a>.</p>'
    )
    led.off()
    return render_template("about.html", content=about_text)


@app.route("/now")
def now():
    led.on()
    weather = "sonnig"
    now_data = get_now()
    now_plot = plot_now()
    content = Markup(
        "<p>Es ist aktuell {} bei {} Grad Celsius, einer Luftfeuchtigkeit von {}% "
        "und einem Luftdruck von {} mbar.</p>".format(
            weather,
            round(
                (now_data["temperature_1"] + now_data["temperature_2"]) / 2, 1
            ),
            now_data["humidity"],
            now_data["pressure"],
        )
        + '<p><img src="{}" alt="Current weather data" width=805px height=500px/></p>'.format(
            now_plot
        )
    )
    led.off()
    return render_template("now.html", content=content)


@app.route("/data")
def data():
    led.on()
    content = Markup(
        "<p>Get past weather data.</p>"
        '<p>Get current weather <a href="/now">here</a>.</p>'
    )
    led.off()
    return render_template("data.html", content=content)


@app.route("/forecast")
def forecast():
    led.on()
    content = Markup(
        "<p>Get weather forecast</p>"
        '<p>Get current weather <a href="/now">here</a>.</p>'
    )
    led.off()
    return render_template("forecast.html", content=content)
