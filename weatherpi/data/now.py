from weatherpi import weather_db as wdf
import numpy as np
import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt
from weatherpi.weather_db import db_manager
from weatherpi.plot import Plotter
import pendulum


def plot_now():
    dbm = db_manager(db_path="weatherpi.db")
    data = dbm.get_weather(
        ["temperature_1", "temperature_2", "time", "humidity", "pressure"],
        time=pendulum.now().subtract(days=1),
    )

    plotter = Plotter(211)
    plotter.add_xaxis(data["time"])
    plotter.add_temp(data["temperature_1"], "temp 1")
    plotter.add_temp(data["temperature_2"], "temp 2")
    plotter.add_sub_plot()
    plotter.add_press(data["humidity"], "Dortmund")
    plotter.add_twinx_plot()
    plotter.add_hum(data["pressure"], "Dortmund")

    path = "static/now.png"
    plotter.save(path)

    return path


def get_now():
    params = ["temperature_1", "temperature_2", "humidity", "pressure"]
    dbm = db_manager(db_path="weatherpi.db")
    data_dict = dbm.get_weather(params)

    try:
        for par in params:
            data_dict[par] = data_dict[par][-1]
    except IndexError as e:
        print(e)
        print("No datapoints in database")
        for par in params:
            data_dict[par] = 0

    return data_dict
