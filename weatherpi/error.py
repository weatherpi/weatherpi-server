class WeatherPiError(Exception):
    """Something goes wrong with your weather Py"""


class SensorError(WeatherPiError):
    """ Something goes wrong wit some sensor."""

    def __init__(self, sensor, msg=None):
        if msg is None:
            # Set some default useful error message
            msg = "An error occured with sensor %s" % sensor
        super(WeatherPiError, self).__init__(msg)
        self.sensor = sensor


class NoConnectionError(WeatherPiError):
    def __init__(self, msg=None):
        if msg is None:
            msg = "Could not connect to server to upload photo."
        super(WeatherPiError, self).__init__(msg)

class TooDarkWarning(WeatherPiError):
    def __init__(self, msg=None):
        if msg is None:
            msg = "It is too dark to take a photo."
        super().__init__(msg)
