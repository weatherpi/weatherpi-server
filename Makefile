all: venv
	@echo "!!! IMPORTANT !!!"
	@echo "first run   'source venv/weatherpi/bin/activate'"
	@echo "then run    'make install'"

venv:
	python3 -m venv venv --prompt "weatherpi-server"

.PHONY: install
install: venv
	pip install -e .
	pip install -r requirements.txt
