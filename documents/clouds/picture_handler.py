from urllib import request
from PIL import Image
import os
import fire
import re
import numpy as np

cloudes = {
    'cirrus':[
        'https://upload.wikimedia.org/wikipedia/commons/d/d0/Cirren_von_Cumulonimbus-Amboss_und_Cu%26Sc.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/7/73/Cirrus_clouds2.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/a/a0/Alentejo_in_HDR_%286170652634%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/4/40/De_Madrid_al_cielo_59c.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/1/1e/Cirrus_23_juni_2008.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/7/7a/Cielo_con_nubes2.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/1/1f/Cielo_con_nubes.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/7/73/Ciel_bleu_et_volute_de_nuage.jpg',
        ],
    'cirrocumulus':[
        'https://upload.wikimedia.org/wikipedia/commons/b/b3/Cirrocumulus_clouds_Thousand_Oaks_July_2010.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/5/59/Cirrocumulus_clouds_over_Bergsfjorden%2C_Senja%2C_Troms%2C_Norway%2C_2015_September.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/7/7f/Cirrocumulus_to_Altocumulus.JPG',
        'http://eo.ucar.edu/staff/rrussell/beta/wx_exhibit/images/cirrocumulus_clouds_di01716_1040x780.jpg',
        'http://www.patternpictures.com/wp-content/uploads/2014/01/PP21571505-Cirrocumulus-Clouds-Sky.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D0%A0%D0%B0%D0%B4%D1%83%D0%B6%D0%BD%D1%8B%D0%B5_%D0%BE%D0%B1%D0%BB%D0%B0%D0%BA%D0%B0.jpg',
	'https://upload.wikimedia.org/wikipedia/commons/b/b2/Cirrocumulus_castellanus_undulatus_and_Cirrocumulus_floccus.jpg',
	'https://upload.wikimedia.org/wikipedia/commons/1/1c/Lenticular_clouds_and_Mount_Hotaka_from_Mount_Otensho_1994-06-25.jpg',
        ],
    'cirrostratus':[
        'https://upload.wikimedia.org/wikipedia/commons/f/fb/St_Aubins_Bay_%284854581783%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/3/3b/Cirrostratus02.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/f/fb/St_Aubins_Bay_%284854581783%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/3/32/Cirroestratos_Fibratus_con_halo_solar_%281%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/0/0e/02162008_Interstate80NWUtah.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/8/80/Big_sky_in_Sao_Torpes_%287984742507%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/9/97/Calahonda_beach_at_Calahonda%2C_Spain_2005_1.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/2/2c/Con_trails_and_clouds_from_the_Woodland_Trust_wood_Theydon_Bois_Essex_England.JPG',
        ],
    'altocumulus':[    
	'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Altocumulus_clouds_-_2004.jpg/1024px-Altocumulus_clouds_-_2004.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/5/56/Altocumulus-Castellanus.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/4/40/Altocumulus_castellanus_undulatus%2C_Altocumulus_floccus_and_Cirrocumulus_floccus.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/b/b5/Mackerelskybig.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/e/e6/Altocumulus_clouds_-_2004.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/7/72/Hiranandani-Gardens-3.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/c/cc/Altocumulus_over_Warsaw_2%2C_Poland%2C_June_26%2C_2005.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/0/05/Himmel_ueber_dem_Bielersee03.jpg',
        ],
    'altostratus':[
	'https://upload.wikimedia.org/wikipedia/commons/a/a3/As_1.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/d/df/2014-11-04_15_31_31_Stratiform_clouds_in_Elko%2C_Nevada.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/a/a0/Sunshine_over_the_Baltic_%286719488887%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/f/f8/Altostratus_translucidus_%282005%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/4/48/Altostratus_undulatus.jpg',
        'http://www.cepolina.com/photo/nature/clouds/medium-level-clouds/altostratus/4/altostratus-sea-horizon.jpg',
        'http://www.cloudman.com/gallery1/photos/143.jpg',
        'http://www.wolken-online.de/images/wolken/altostratus/altostratus_translucidus.jpg',
        ],
    'stratocumulus':[
	'https://upload.wikimedia.org/wikipedia/commons/8/89/Large_Stratocumulus.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/0/02/Stratocumulus_castellanus.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/c/c4/Stratocumulus_castellanus_2.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/f/f4/Flickr_-_Nicholas_T_-_Passing_Through.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/c/c5/Flair..jpg',
        'https://upload.wikimedia.org/wikipedia/commons/9/9f/Convective_roll_clouds.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/d/d2/Clouds_Bonheiden_Belgium_jun_2014.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/9/9e/Low_lying_clouds_over_hills_near_swifts_creek.jpg',
        ],
    'stratus':[
	'https://upload.wikimedia.org/wikipedia/commons/2/21/High_Stratus_nebulosus.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/2/2b/Bodennebel_beleuchtet.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/8/87/Clouds_CL6.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/d/d1/Stratus-Opacus-Uniformis.jpg',
        'http://static.wixstatic.com/media/6bc624_2a08227c5c694e73b8c80ad3ec7bc714~mv2_d_4912_3264_s_4_2.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/3/3f/AlpineRainbow.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/3/36/Cloudy_scenes.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/f/fc/Camino_al_salto_del_tabaquillo.JPG',
        ],
    'cumulus':[
	'https://upload.wikimedia.org/wikipedia/commons/6/66/Img20050526_0007_at_tannheim_cumulus.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/3/3c/GoldenMedows.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/6/6d/Frayed_Cu_med_above_Oltschiburg.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/4/4a/D%C3%BCnenkette_Juist.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/c/c3/Cumulus_con_above_Brienzersee.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/f/f8/Cumulus_behind_hill.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/8/87/N%C3%B6tsch_Kerschdorf_Filialkirche_hl._Nikolaus_und_Friedhof_08052015_3451.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/4/40/Cumulus_congestus_touching_Cc.JPG',
        ],
    'nimbostratus':[
	'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Ns1.jpg/800px-Ns1.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/d/d8/Rainbow_in_front_of_Ns_in_evening.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/a/ac/Nimbostratus_and_air_current.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/7/7b/Nimbostratus_Clouds_Virginia_USA.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/4/44/K%26A_Canal_%284394833686%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/2/27/Nimbostratus_praecipitatio.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/e/e6/Nimbostratus_over_Tallinn.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/0/04/Nimbostratus_01.jpg'
        ],
    'cumulonimbus':[
	'https://upload.wikimedia.org/wikipedia/commons/8/88/Cumulonimbus_mit_Riesenamboss.JPG',
        'https://upload.wikimedia.org/wikipedia/commons/a/a7/Anvil_shaped_cumulus_panorama_edit.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/5/5f/Haltschule_-_Sunset_Thunderhead_%28by%29.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/9/98/Roll-Cloud-Racine.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/2/2d/Markante_Gewitterzelle_am_16._August_2009_%C3%BCber_dem_Petersberg_-_panoramio.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/a/a9/2016-07-19_14_22_46_Thunderstorm_cirrus_anvil_with_mammatus_hanging_down_above_U.S._Route_211_eastbound_and_U.S._Route_340_northbound_%28Lee_Highway%29_at_General_Drive_just_west_of_Luray_in_Page_County%2C_Virginia.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/6/63/Gewitterwolke_superzelle.jpg',
        'https://upload.wikimedia.org/wikipedia/commons/c/c4/Cloud_over_Boulder_Mtn_%283684617703%29.jpg',
        ],
    }

class picture_handler:
    def __init__(self, path='../../production/cloudsheet/pictures/'):
        self.path = path

    def process_pic(self, url, path, res=(256,256)):
        request.urlretrieve(url, path)
        img = Image.open(path)
        size = min(img.size[0], img.size[1])
        img = img.crop((0, 0, size, size))
        img.thumbnail(res)
        img.save(path)

    def downl_gallery(self, dic=cloudes):
        if not os.path.exists(self.path):
            add = input('Path do not exist. \nDo you want to add it? (y/n): ')
            if(add == 'y'):
                os.makedirs(self.path)
        for cloud_type in cloudes:
            print('Download pictures from cloud type {}'.format(cloud_type))
            i = 0
            for url in cloudes[cloud_type]:
                i += 1
                self.process_pic(url, self.path+cloud_type+str(i)+'.jpg')
                print('Downloades picture {}'.format(i))

    def shuffel_name(self):
        filenames = np.array(os.listdir(self.path))
        Filenames = np.array(filenames)
        for x in range(len(filenames)):
            Filenames[x] = filenames[x].replace('.jpg', '').replace('\d', '')
            Filenames[x] = re.sub('\d', '', Filenames[x])
        for x in np.unique(np.array(Filenames)):
            old_file = filenames[Filenames == x]
            pos = np.random.permutation(len(old_file))
            for change_file, pos in zip(old_file, pos):
                os.rename(self.path+change_file, self.path + old_file[pos] +
                '.tpp')
        for x in filenames:
            os.rename(self.path + x + '.tpp', self.path+x)


if __name__ == '__main__':
    fire.Fire(picture_handler)
