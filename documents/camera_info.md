Aim is to make picture from the sky with camera, to forecast the clouds.
Maybe is it posibile to get extract further information from nature.

# Installation
Installation of the camera module is quite simple.
First start the **raspi-config** and configure a connection to a peripher.
```bash
sudo raspi-config
```
Then activate the camera and after reboot you are able to make a testshoot 
```bash
raspistill -v -o test.jpg
```

