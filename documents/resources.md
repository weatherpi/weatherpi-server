# Resources

## Links
- [GPIO Pins][gpio]
- [camera package][picamera]
- [RFID][rfid]
- [face Rocognition Python][facerecog]
- [rest api eve][eve]
- [django tutorial][djangobook]
- [sql alchemy][sqlalchemy]


[gpio]: https://www.raspberrypi.org/documentation/usage/gpio-plus-and-raspi2/README.md "raspberrypi.org"
[picamera]: http://picamera.readthedocs.io/ "readthedocs.io"
[rfid]: https://pimylifeup.com/raspberry-pi-rfid-rc522/ "pimpmylifeup.com"
[facerecog]: https://github.com/ageitgey/face_recognition "github.com"
[eve]: http://python-eve.org/ "eve"
[djangobook]: http://books.agiliq.com/projects/django-api-polls-tutorial/en/latest/ "books.agiliq.com"
[sqlalchemy]: https://www.pythoncentral.io/overview-sqlalchemys-expression-language-orm-queries/ "pythoncentral.com"
