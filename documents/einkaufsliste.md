# Einkaufsliste

- [Thermometer und Luftfeuchtigkeits Messer][1]
- [Barometer][2]
- [Kamera][3]
- [Widerstaende][4]




[1]: https://www.amazon.de/IZOKEE-Digital-Temperatur-Luftfeuchtigkeitssensor-Raspberry/dp/B077PR7DXT/ref=sr_1_6?ie=UTF8&qid=1520449568&sr=8-6&keywords=raspberry%2Bthermometer&th=1 "Amazon.de"
[2]: https://www.amazon.de/BMP180-Sensor-Temperatur-Erkennung-Optimus-Elektrische/dp/B0722MC2G6/ref=sr_1_cc_5?s=aps&ie=UTF8&qid=1520504740&sr=1-5-catcorr&keywords=raspberry+barometer "Amazon.de"
[3]: https://www.amazon.de/Raspberry-Kamera-Modul-mit-Filter/dp/B01ER4FA9U/ref=sr_1_5?s=ce-de&ie=UTF8&qid=1520505310&sr=1-5&keywords=raspberry+kamera "Amazon.de"
[4]:https://www.amazon.de/Elegoo-Widerst%C3%A4nde-Sortiment-St%C3%BCck-Metallfilm/dp/B072BHDBDG/ref=sr_1_1?s=ce-de&ie=UTF8&qid=1520505447&sr=1-1&keywords=wiederst%C3%A4nde+set "Amazon.de"
